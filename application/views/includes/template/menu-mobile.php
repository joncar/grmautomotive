<!--Menu Mobile-->
<div class="menu-wrap page-scroll">
    <div class="main-menu">
        <h4 class="title-menu">Main menu</h4>
        <button class="close-button" id="close-button">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <ul class="nav-menu">
        <!--<li class="selected active"><a href="#">Home</a>
            <ul class="child-nav dropdown-nav">
                <li><a href="index-v2.html">Home V2</a></li>
                <li><a href="index-onepage.html">One Page</a></li>
                <li><a href="index-layer.html">Layer Slider</a></li>
                <li><a href="index-shop.html">E-commerce</a></li>
                <li><a href="index-store.html">Store</a></li>
                <li><a href="index-sport-club.html">Sport Club</a></li>
                <li><a href="index-boxing.html">Boxing</a></li>
                <li><a href="index-white.html">Light version</a></li>
                <li><a href="coming-soon.html">Under Construction</a></li>
            </ul>
        </li>-->
        <li class="selected active"><a href="<?= site_url() ?>#home">Home</a></li>
        <li>
            <a href="<?= site_url() ?>#instalacions">Instalacions</a>
            <ul class="child-nav dropdown-nav">
                <li><a href="<?= site_url('p/packs') ?>">Partido Bubble</a></li>
                <li><a href="<?= site_url('p/packs') ?>">Empresas</a></li>
                <li><a href="<?= site_url('p/packs') ?>">Cumpleaños</a></li>
                <li><a href="<?= site_url('p/packs') ?>">Despedidas</a></li>
            </ul>
        </li>
        <li><a href="<?= site_url() ?>#packs">Packs</a></li>
        <li><a href="<?= site_url() ?>#agencia">Agencia</a></li>
        <li><a href="<?= site_url() ?>#contacte">Contacte</a></li>
    </ul>
</div>
<!--Menu Mobile-->
