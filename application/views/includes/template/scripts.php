<script>
    var URL = '<?= base_url() ?>';
</script>
<script type="text/javascript" src="<?= base_url() ?>libs/jquery/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/smoothscroll/SmoothScroll.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/superfish-menu/js/superfish.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/jquery-ui/external/touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/jquery-ui/external/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/parallax/parallax.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/jquery-countto/jquery.countTo.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/jquery-appear/jquery.appear.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/as-pie-progress/jquery-asPieProgress.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/caroufredsel/helper-plugins/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/caroufredsel/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/isotope/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/isotope/fit-columns.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/slick-sider/slick.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/lwt-countdown/jquery.lwtCountdown-1.0.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/swiper-sider/dist/js/swiper.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/jplayer/dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>libs/jquery-cookie/jquery.cookie.min.js"></script>

<?php if(!empty($scripts)){ echo $scripts; } ?>

<!-- Theme Script-->
<script type="text/javascript" src="<?= base_url() ?>js/template/main.js"></script>
<script>
    function subscribir(){
      $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:$("#emailSub").val()},function(data){
          $("#subsmessage").html(data);
          $("#subsmessage").show();
      });
      return false;
    }
    
    
    if(typeof(localStorage.compare)==='undefined'){
        localStorage.compare = '[]';
    }    
    $(document).on('ready',function(){
        convertirlinkcomparar();
        $(document).on('click','.btn-compare',function(){
           var cmp = $(this).data('id');
           comparar = JSON.parse(localStorage.compare);
           var encontrado = false;
           for(var i in comparar){
               if(comparar[i] === cmp){
                   encontrado = true;
               }
           }
           if(!encontrado){
                comparar.push(cmp);
            }           
           localStorage.compare = JSON.stringify(comparar);
           console.log(localStorage.compare);
           convertirlinkcomparar();
            $(this).addClass('active');
            
            var post = $(this).closest('.post');
            var messageAction = post.find('.content-message .message-inner');
            var htmlString =  '<i class="fa fa-check mv-color-primary"></i> 1 producto anadido a la lista de comparacion. <a href="'+$("#compararlink").attr('href')+'"><strong>Ver lista de comparacion</strong></a>';
            messageAction.html(htmlString).hide().fadeIn();
            post.find('.patternproduct').show();
        });
        
        $(document).on('click','.btn-del-to-compare',function(){
           var cmp = $(this).data('id');
           var comparar = JSON.parse(localStorage.compare);
           var i = comparar.indexOf(cmp);
            if(i != -1) {
                    comparar.splice(i, 1);
            }
           localStorage.compare = JSON.stringify(comparar);
           convertirlinkcomparar();
           document.location.href=$("#compararlink").attr('href');
        });
    });
    
    function convertirlinkcomparar(){
        var cmplink = $("#compararlink");
        var compare = JSON.parse(localStorage.compare);
        var str = '';
        for(var i in compare){
            str+= compare[i]+'-';
        }
        cmplink.attr('href','<?= base_url() ?>compare/'+str);
    }
</script>
<!-- .mv-main-body-->
<script>
    $(document).on('change','.cantidad',function(){
        var obj = $(this);
        obj.attr('disabled',true);
        $.ajax({
            url:'<?= base_url('carrito/frontend/presupuesto/update/') ?>/'+obj.data('pre'),
            type:'post',
            data:{cantidad:obj.val(),productos_id:obj.data('id')},
            success:function(data){
                obj.attr('disabled',false);
            }
        });
    });
</script>
