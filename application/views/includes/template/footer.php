<section class="main-newsletter mv-newsletter-style-1 mv-wrap">
    <div style="background-image: url(<?= base_url() ?>img/background/newsletter.jpg)" class="newsletter-bg">
        <div class="container">
            <form method="post" onsubmit="return subscribir()" class="form-newsletter">
                <label class="hidden-xs mv-label text-right text-uppercase">Newsletter</label>
                <div id="subsmessage" style="display:none;"></div>
                <div class="mv-field">
                    <div class="mv-inputbox-group">
                        <input type="text" name="test138" id="emailSub" placeholder="Coloca tu email aqui" data-mv-placeholder="Subscríbite para recibir noticias " class="mv-inputbox mv-inputbox-style-3"/>
                        <div class="inputbox-addon">
                            <button type="submit" class="mv-btn mv-btn-block mv-btn-style-13"><span class="btn-icon fa fa-long-arrow-right"></span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- .mv-newsletter-style-1-->

<footer class="footer mv-footer-style-2 mv-wrap">
    <div style="background-image: url(<?= base_url() ?>img/background/demo_bg_1920x1200.jpg)" class="footer-bg">
        <div class="container">
            <div class="footer-inner">
                <div id="footerNav" role="tablist" aria-multiselectable="true" class="footer-nav panel-group">
                    <div class="row">
                        <div class="col-md-3 footer-nav-col footer-contact"><a data-toggle="collapse" data-parent="#footerNav" href="#footerContact" aria-expanded="true" aria-controls="footerContact" class="footer-title collapsed">info de contacto</a>
                            <div id="footerContact" role="tabpanel" class="footer-main collapse">
                                <ul class="mv-ul footer-main-inner list">
                                    <li class="mv-icon-left-style-1 item">
                                        <div class="mv-dp-table align-middle">
                                            <div class="mv-dp-table-cell icon"><i class="icon-fa fa fa-map-marker mv-f-22 mv-color-primary" style="color: #f59b2e;"></i></div>
                                            <div class="mv-dp-table-cell text">C/ Gran Bretaña, 22 Nave 3 08700 IGUALADA - BCN- Spain-</div>
                                        </div>
                                    </li>
                                    <li class="mv-icon-left-style-1 item">
                                        <div class="mv-dp-table align-middle">
                                            <div class="mv-dp-table-cell icon"><i class="icon-fa fa fa-mobile mv-f-26 mv-color-primary"style="color: #f59b2e;"></i></div>
                                            <div class="mv-dp-table-cell text">TELÉFONO: (+34)  607 719 378 <br>WATSAPP: (+34)  607 719 378</div>
                                        </div>
                                    </li>
                                    <li class="mv-icon-left-style-1 item">
                                        <div class="mv-dp-table align-middle">
                                            <div class="mv-dp-table-cell icon"><i class="icon-fa fa fa-envelope-o mv-f-20 mv-color-primary" style="color: #f59b2e;"></i></div>
                                            <div class="mv-dp-table-cell text">EMAIL:<a href="mailto:info@Juxdesign.com"> info@grmautomotive-eu</a></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 footer-nav-col footer-about-us"><a data-toggle="collapse" data-parent="#footerNav" href="#footerAboutUs" aria-expanded="false" aria-controls="footerAboutUs" class="footer-title collapsed">grm automotive</a>
                            <div id="footerAboutUs" role="tabpanel" class="footer-main collapse">
                                <div class="footer-main-inner">
                                    <div class="about-us-content">
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.</p>
                                        <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 footer-nav-col footer-faqs"><a data-toggle="collapse" data-parent="#footerNav" href="#footerFaqs" aria-expanded="false" aria-controls="footerFaqs" class="footer-title collapsed">faqs</a>
                            <div id="footerFaqs" role="tabpanel" class="footer-main collapse">
                                <ul class="mv-ul footer-main-inner list">
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Contacto</span></a></li>
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Devoluciones</span></a></li>
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Envios</span></a></li>
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Marcas</span></a></li>
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Ofertas</span></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 footer-nav-col footer-order-tracking"><a data-toggle="collapse" data-parent="#footerNav" href="#footerOrderTracking" aria-expanded="false" aria-controls="footerOrderTracking" class="footer-title collapsed">Pedidos Traking</a>
                            <div id="footerOrderTracking" role="tabpanel" class="footer-main collapse">
                                <ul class="mv-ul footer-main-inner list">
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Empresa</span></a></li>
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Devoluciones</span></a></li>
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Contáctenos</span></a></li>
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Terminos y Conddiciones</span></a></li>
                                    <li class="item mv-icon-left-style-2"><a href="#"><span class="text"><span class="icon"><i class="fa fa-angle-right"></i></span>Politica de Privacidad</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .footer-nav-->

                <div class="footer-payment">
                    <ul class="mv-ul list">
                        <li class="item"><a href="#"><img src="<?= base_url() ?>img/icon/icon_paypal.png" alt="icon"/></a></li>
                        <li class="item"><a href="#"><img src="<?= base_url() ?>img/icon/icon_master_card.png" alt="icon"/></a></li>
                        <li class="item"><a href="#"><img src="<?= base_url() ?>img/icon/icon_american_express.png" alt="icon"/></a></li>
                        <li class="item"><a href="#"><img src="<?= base_url() ?>img/icon/icon_visa.png" alt="icon"/></a></li>
                    </ul>
                </div>
                <!-- .footer-payment-->

                <div class="footer-copyright text-center">Copyright &copy; 2017 GRM Automotive by<a href="http://www.hipo.tv/" target="_blank"> hipo.</a> Todos los Derechos Reservados.</div>
                <!-- .footer-copyright-->
            </div>
        </div>
    </div>

    <button type="button" class="mv-btn mv-btn-style-17 mv-back-to-top fixed-right-bottom"><i class="btn-icon fa fa-long-arrow-up"></i></button>
</footer>
<!-- .footer-->

<div class="off-canvas-wrapper-left hidden-md hidden-lg">
    <div class="off-canvas-left">
        <div class="off-canvas-header">
            <button class="btn-close-off-canvas click-close-off-canvas">x</button>
        </div>
        <div class="off-canvas-body">
            <nav class="main-nav">
                <ul class="nav sf-menu expand-all">
                    <li class="mega-columns">
                        <a href="<?= site_url() ?>">
                            <span class="menu-text">Home</span>
                        </a>                        
                    </li>
                    <li><a href="<?= site_url('p/empresa') ?>"><span class="menu-text">Empresa</span></a></li>
                    <li><a href="<?= site_url('catalogo') ?>"><span class="menu-text">Catalogo <i class="menu-icon fa fa-angle-down"></i></span></a>
                        <ul>
                            <?php foreach($this->db->get('categoria_productos')->result() as $c): ?>
                                <li><a href="<?= site_url('productos/'.toUrl($c->id.'-'.$c->categoria_nombre)) ?>"><?= $c->categoria_nombre ?></a></li>
                            <?php endforeach ?>                            
                        </ul>
                    </li>
                    <li><a href="<?= site_url('blog') ?>"><span class="menu-text">Blog</span></a></li>
                    <li><a href="<?= site_url('p/faq') ?>"><span class="menu-text">FAQ</span></a></li>
                    <li><a href="<?= site_url('p/contacto') ?>"><span class="menu-text">Contacto</span></a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- .off-canvas-wrapper-left-->
