<header class="header mv-header-style-5 mv-wrap">
    <div class="header-top">
        <div class="container">
            <div class="header-top-inner">
                <div class="mv-col-wrapper">
                    <div class="mv-col-left header-contact">
                        <ul class="mv-ul clearfix group-contact hidden-xs">
                            <li class="item-button mv-icon-left-style-2">
                                <span class="text"> 
                                    <span class="icon">
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                    Soporte 24/7 
                                </span>
                            </li>
                            <li class="item-button mv-icon-left-style-2">
                                <a href="tel:(+800)123456789" class="text"> 
                                    <span class="icon">
                                        <i class="fa fa-phone"></i>
                                    </span>
                                    Teléfono: (+34) 607 719 378
                                </a>
                            </li>
                        </ul>
                        <ul class="mv-ul clearfix group-language">
                            <li class="item-button dropdown mv-dropdown-style-1 item-currency">
                                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mv-btn mv-btn-style-24">
                                    <i class="fa fa-eur"></i>&nbsp; EUR<i class="icon fa fa-angle-down"></i>
                                </button>
                            </li>
                            <li class="item-button dropdown mv-dropdown-style-1 item-language">
                                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mv-btn mv-btn-style-24">
                                    <img src="<?= base_url() ?>img/icon/icon_flag_spain.png" alt="icon"/>
                                    <span class="btn-text hidden-xs hidden-sm">&nbsp; Castellano</span>
                                    <!--<i class="icon fa fa-angle-down"></i>-->
                                </button>
                            </li>
                        </ul>
                    </div>
                    <!-- .header-contact-->

                    <div class="mv-col-right align-bottom header-account">
                        <div class="mv-btn-group text-right">
                            <div class="group-inner">
                                <div class="item-button mv-btn dropdown mv-dropdown-style-1">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="mv-btn mv-btn-style-12">
                                        <span class="btn-text hidden-xs hidden-sm"><?= empty($_SESSION['nombre'])?'Mi cuenta':'Hola '.$_SESSION['nombre'] ?></span>
                                        <i class="btn-icon fa fa-user hidden-md hidden-lg"></i>
                                        <i class="icon fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-menu pull-right">
                                        <div class="dropdown-menu-inner"> 
                                            <nav class="mv-menu-style-3">
                                                <ul>
                                                    <li>
                                                        <a href="<?= site_url('presupuesto') ?>">                                                            
                                                            Mi presupuesto
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= site_url('compare') ?>" id="compararlink">                                                            
                                                            Comparar
                                                        </a>
                                                    </li>
                                                    <?php if(empty($_SESSION['user'])): ?>
                                                        <li>
                                                            <a href="<?= site_url('panel') ?>">
                                                                <i class="fa fa-sign-in mv-color-primary"></i>&nbsp;
                                                                Entrar
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= site_url('panel') ?>">
                                                                <i class="fa fa-pencil-square-o mv-color-primary"></i>&nbsp;
                                                                Registrar
                                                            </a>
                                                        </li>
                                                    <?php else: ?>
                                                        <li>
                                                            <a href="<?= site_url('wishlist') ?>">Mis preferidos</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= site_url('main/unlog') ?>">Salir</a>
                                                        </li>
                                                    <?php endif ?>                                                    
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .header-account-->
                </div>
            </div>
        </div>
    </div>
    <!-- .header-top-->

    <div class="header-main-nav mv-fixed-enabled">
        <div class="container">
            <div class="container-inner">
                <div class="header-toggle-off-canvas hidden-md hidden-lg">
                    <button type="button" class="mv-btn mv-btn-style-5 btn-off-canvas-toggle click-btn-off-canvas-left">
                        <i class="icon fa fa-bars"></i>
                    </button>
                </div>
                <!-- .header-toggle-off-canvas-->

                <div class="header-logo">
                    <a href="<?= site_url() ?>" title="Motor Vehikal">
                        <img src="<?= base_url() ?>img/logo/logo_5.png" alt="logo" class="logo-img img-default image-live-view"/>
                        <img src="<?= base_url() ?>img/logo/logo_2.png" alt="logo" class="logo-img img-scroll image-live-view"/>
                    </a>
                </div>
                <!-- .header-logo-->

                <div class="main-nav-wrapper hidden-xs hidden-sm">
                    <nav class="main-nav">
                        <ul class="nav sf-menu">
                            <li class="mega-columns">
                                <a href="<?= site_url() ?>">
                                    <span class="menu-text">
                                        Home <!--<i class="menu-icon fa fa-angle-down"></i>-->
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= site_url('p/empresa') ?>">
                                    <span class="menu-text">
                                        Empresa 
                                    </span>
                                </a>                                
                            </li>
                            <li>
                                <a href="#">
                                    <span class="menu-text">Catalogo <i class="menu-icon fa fa-angle-down"></i></span>
                                </a>
                                <ul>
                                    <?php foreach($this->db->get('categoria_productos')->result() as $c): ?>
                                        <li><a href="<?= site_url('productos/'.toUrl($c->id.'-'.$c->categoria_nombre)) ?>"><?= $c->categoria_nombre ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </li>
                            <li>
                                <a href="<?= site_url('blog') ?>">
                                    <span class="menu-text">Blog</span>
                                </a>
                            </li>
                            <li><a href="<?= site_url('p/faq') ?>">
                                    <span class="menu-text">FAQ</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= site_url('p/contacto') ?>">
                                    <span class="menu-text">Contacto</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- .header-main-nav-->

                <div class="header-right-button">                    

                    <div class="header-shop">
                        <div class="item-button">
                            <a href="<?= base_url('wishlist') ?>" class="mv-btn mv-btn-style-11 btn-my-wishlist preferenciasicon">
                                <span class="btn-inner">
                                    <span class="icon fa fa-heart-o"></span>
                                    <span class="number"><?= empty($_SESSION['user'])?0:$this->user->wishlist->num_rows(); ?></span>                                    
                                </span>
                            </a>
                        </div>
                        <?php /*$this->load->view('includes/template/_carrito')*/ ?>
                        <div class="item-button">
                            <a href="<?= base_url('presupuesto') ?>" class="mv-btn mv-btn-style-11 btn-my-wishlist presupuestoicon">
                                <span class="btn-inner">
                                    <span class="icon fa fa-calculator"></span>
                                    <span class="number"><?= $_SESSION['itempresupuesto']; ?></span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <!-- .header-shop-->
                </div>
            </div>
        </div>
    </div>
    <!-- .header-main-nav-->
</header>
<!-- .header-->
