<div class="item-button dropdown mv-dropdown-style-1 script-dropdown-1">
    <button type="button" class="mv-btn mv-btn-style-11 btn-dropdown btn-my-cart">
        <span class="btn-inner">
            <span class="icon fa fa-calculator"></span>
            <span class="number">0</span>                                        
        </span>
    </button>
    <div class="dropdown-menu pull-right">
        <div class="dropdown-menu-inner">
            <div class="mv-block-style-39">
                <div class="block-39-header">Tienes <span class="mv-color-primary">0 articulos</span> en tu carrito de compras</div>
                <div class="block-39-list">

                    <!--
                    <article class="item post">
                        <div class="item-inner">
                            <div class="mv-dp-table align-top">
                                <div class="mv-dp-table-cell block-39-thumb">
                                    <div class="thumb-inner mv-lightbox-style-1">
                                        <a href="#" title="Richa Rock Glove">
                                            <img src="<?= base_url() ?>img/demo/demo_80x100.png" alt="demo" class="block-39-thumb-img"/>
                                        </a>
                                    </div>
                                </div>
                                <div class="mv-dp-table-cell block-39-main">
                                    <div class="block-39-main-inner">
                                        <div class="block-39-title">
                                            <a href="product-detail.html" title="Richa Rock Glove" class="mv-overflow-ellipsis">
                                                Richa Rock Glove
                                            </a>
                                        </div>
                                        <div class="block-39-price"> 
                                            <div class="new-price">$200,00</div>
                                        </div>
                                        <ul class="block-24-meta mv-ul">
                                            <li class="meta-item mv-icon-left-style-3">
                                                <span class="text">Cantidad: 2</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <button type="button" title="Remove from Wishlist" class="mv-btn mv-btn-style-4 fa fa-close btn-delete-product"></button>
                        </div>
                    </article>                                                                                       
                    <!-- .post-->
                </div>

                <!--<div class="block-39-total mv-col-wrapper">
                    <div class="mv-col-left">total</div>
                    <div class="mv-col-right">0</div>
                </div>

                <div class="block-39-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="#" class="mv-btn mv-btn-style-5 btn-5-h-45 btn-5-gray responsive-btn-5-type-2 mv-btn-block">Ver Carro</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" class="mv-btn mv-btn-style-5 btn-5-h-45 responsive-btn-5-type-2 mv-btn-block">Procesar</a>
                        </div>
                    </div>
                </div>
            </div>
                <!-- .mv-block-style-39-->
            </div>
        </div>
    </div>
</div>
