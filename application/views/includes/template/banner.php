<!--Banner-->
<section class="slide-container to-top">
    <div class="ms-fullscreen-template" id="slider1-wrapper">
        <!-- masterslider -->
        <div class="master-slider ms-skin-default" id="masterslider-index">
            <div class="ms-slide slide-1" data-delay="0">
                <div class="slide-pattern"></div>
                <img src="<?= base_url() ?>img/blank.gif" data-src="<?= base_url() ?>img/bg_3.jpg" alt="lorem ipsum dolor sit"/>
                <h3 class="ms-layer hps-title1" style="left:95px"
                    data-type="text"
                    data-ease="easeOutExpo"
                    data-delay="1000"
                    data-duration="0"
                    data-effect="skewleft(30,80)">
                    Athlete Fitness Club
                </h3>																												
                <h3 class="ms-layer hps-title3" style="left:91px"
                    data-type="text"
                    data-delay="1900"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,40,t)"
                    data-ease="easeOutExpo">
                    Make You Be The Fighter
                </h3>

                <h3 class="ms-layer hps-title4" style="left:95px"
                    data-type="text"
                    data-delay="2500"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,18,t)"
                    data-ease="easeOutExpo">
                    Try A Free Class
                </h3>
            </div>
            <div class="ms-slide slide-2" data-delay="0">
                <div class="slide-pattern"></div>							  
                <img src="<?= base_url() ?>img/blank.gif" data-src="<?= base_url() ?>img/bg_2.jpg" alt="lorem ipsum dolor sit"/>
                <h3 class="ms-layer hps-title1" style="left:95px"
                    data-type="text"
                    data-ease="easeOutExpo"
                    data-delay="1000"
                    data-duration="0"
                    data-effect="skewleft(30,80)">
                    Athlete Fitness Club
                </h3>																												
                <h3 class="ms-layer hps-title3" style="left:91px"
                    data-type="text"
                    data-delay="1900"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,40,t)"
                    data-ease="easeOutExpo">
                    Make You Be The Fighter
                </h3>

                <h3 class="ms-layer hps-title4" style="left:95px"
                    data-type="text"
                    data-delay="2500"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,18,t)"
                    data-ease="easeOutExpo">
                    Try A Free Class
                </h3>
            </div>	

            <div class="ms-slide slide-3" data-delay="0">
                <div class="slide-pattern"></div>							  
                <img src="<?= base_url() ?>img/blank.gif" data-src="<?= base_url() ?>img/bg-home-v2.jpg" alt="lorem ipsum dolor sit"/>
                <h3 class="ms-layer hps-title1" style="left:95px"
                    data-type="text"
                    data-ease="easeOutExpo"
                    data-delay="1000"
                    data-duration="0"
                    data-effect="skewleft(30,80)"
                    >
                    Athlete Fitness Club
                </h3>																												
                <h3 class="ms-layer hps-title3" style="left:91px"
                    data-type="text"
                    data-delay="1900"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,40,t)"
                    data-ease="easeOutExpo"
                    >
                    Make You Be The Fighter
                </h3>

                <h3 class="ms-layer hps-title4" style="left:95px"
                    data-type="text"
                    data-delay="2500"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,18,t)"
                    data-ease="easeOutExpo"
                    >
                    Try A Free Class
                </h3>
            </div>
            <!--div class="ms-slide slide-4">
               <div class="slide-pattern"></div>
                    <video data-autopause="false" data-mute="true" data-loop="true" data-fill-mode="fill">
                            <source src="<?= base_url() ?>img/video/demo.mp4" type="video/mp4"/>									
                    </video>  
            </div-->
        </div>
        <!-- end of masterslider -->
        <div class="to-bottom" id="to-bottom"><i class="fa fa-angle-down"></i></div>
    </div>
</section>
<!--End Banner-->
