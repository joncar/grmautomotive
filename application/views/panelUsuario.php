<div class="page-title parallax parallax4 panel"  style=' background-size: inherit;'>           
        <div class="container">
            <div class="row">
                <div class="col-md-12">                    
                    <div class="page-title-heading">
                        <h2 class="title">Els meus projectes</h2>
                    </div><!-- /.page-title-heading -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><a href="#">Home</a></li>
                            <li>Els meus projectes</li>
                        </ul>                   
                    </div><!-- /.breadcrumbs --> 
                </div><!-- /.col-md-12 -->  
            </div><!-- /.row -->  
        </div><!-- /.container -->                      
    </div><!-- /page-title parallax -->

    <section class="main-content blog-post v1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?= $output ?>
                </div><!-- /col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->   
    </section><!-- /main-content blog-post -->
