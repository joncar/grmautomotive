<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?= !empty($title)?$title:'GRM-Automotive' ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?= base_url() ?>img/icon/favicon.ico" type="image/x-icon">
    <!-- Web Fonts-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Varela+Round">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <!-- Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>libs/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>libs/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>libs/superfish-menu/css/superfish.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>libs/slick-sider/slick.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>libs/slick-sider/slick-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>libs/swiper-sider/dist/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>libs/magnific-popup/dist/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/main.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/shortcodes.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/home.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/style-selector.css">
    <link id="style-main-color" rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/color/color1.css">        
  </head>
    <body>
        <div class="mv-site">
            <?php $this->load->view('includes/template/header'); ?>
            <?php $this->load->view($view); ?>
            <?php $this->load->view('includes/template/footer'); ?>
        </div>
        <?php $this->load->view('includes/template/modals'); ?>
        <?php $this->load->view('includes/template/scripts'); ?>
    </body>
</html>
