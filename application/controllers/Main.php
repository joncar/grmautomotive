<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {
    //public $theme = 'themes/bristol/';
    public $theme = '';
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('redsysAPI');
        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_ALL, 'ca_ES', 'Catalan_Spain', 'Catalan');
        if(!empty($_SESSION['user'])){
            $this->user->wishlist = $this->db->get_where('wishlist',array('user_id'=>$this->user->id));
            $this->db->update('presupuesto',array('usuario'=>$this->user->id),array('usuario'=>$_SESSION['visitaid']));
        }
        if(empty($_SESSION['visitaid']) ){
            $_SESSION['visitaid'] = date("dhis").rand(0,2048);
        }//Valor temporal para usuarios no registrados  y almacenar en la tabla presupeusto y carrito
        $usuario = empty($_SESSION['user'])?$_SESSION['visitaid']:$_SESSION['user'];
        $_SESSION['itempresupuesto'] = $this->db->get_where('presupuesto',array('usuario'=>$usuario))->num_rows();
    }
    
    
    
    function get_entries(){
        $blog = new Bdsource();
        $blog->limit = array('3','0');
        $blog->order_by = array('fecha','DESC');        
        $blog->init('blog');
        foreach($this->blog->result() as $n=>$b){
            $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
            $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
        }
        if($this->blog->num_rows()>0){
            $this->blog->tags = $this->blog->row()->tags;
        }
    }
    
    function get_productos(){
        $categoria_productos = new Bdsource();         
        $categoria_productos->init('categoria_productos');
        foreach($this->categoria_productos->result() as $n=>$b){
            $this->categoria_productos->row($n)->foto = base_url('img/productos/'.$b->foto);
            $this->db->limit('3');
            $this->db->order_by('posicion_main','ASC');
            $this->db->where('mostrar_en_main',1);
            $productos = $this->db->get_where('productos',array('categoria_productos_id'=>$b->id));
            foreach($productos->result() as $n2=>$b2){
                $productos->row($n2)->link = site_url('producto/'.toURL($b2->id.'-'.$b2->productos_nombre));
                $productos->row($n2)->foto_portada = base_url('img/productos/'.$b2->foto_portada);
                $productos->row($n2)->foto_hover = base_url('img/productos/'.$b2->foto_hover);            
            }            
            $this->categoria_productos->row($n)->productos = $productos;
        }
    }
    
    function get_ofertas(){
        $productos = new Bdsource();        
        $productos->where('oferta',1);
        $productos->init('productos');
        foreach($this->productos->result() as $n=>$b){
            $this->productos->row($n)->link = site_url('producto/'.toURL($b->id.'-'.$b->productos_nombre));
            $this->productos->row($n)->foto_portada = base_url('img/productos/'.$b->foto_portada);
            $this->productos->row($n)->foto_hover = base_url('img/productos/'.$b->foto_hover);        
        }
    }
    

    public function index() {
        $this->get_entries();
        $this->get_productos();
        $this->get_ofertas();
        $this->loadView(array('view'=>'main','blog'=>$this->blog,'categoria_productos'=>$this->categoria_productos,'ofertas'=>$this->productos));
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $this->load->view('template', $param);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
            correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
            correo('info@hipo.tv',$mensaje->titulo,$mensaje->texto);
        }

}
