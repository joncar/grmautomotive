<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        function get_categorias(){
            $this->db->limit('8');
            $categorias = $this->db->get_where('blog_categorias');
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('blog',array('blog_categorias_id'=>$c->id))->num_rows();
            }
            return $categorias;
        }                
        
        public function populares(){
            $blog = new Bdsource();
            $blog->limit = array('2','0');
            $blog->init('blog',FALSE,'populares');
            foreach($this->populares->result() as $n=>$b){
                $this->populares->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                $this->populares->row($n)->foto = base_url('img/blog/'.$b->foto);
            }
            foreach($this->populares->result() as $n=>$b){
                $this->populares->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows;                
            }
            return $this->populares;
        }
        
        public function index(){
            $blog = new Bdsource();
            $blog->where('status',1);
            $blog->limit = array('6','0');
            $blog->order_by = array('visitas','DESC');
            if(!empty($_GET['direccion'])){
                $blog->like('titulo',$_GET['direccion']);
            }
            if(!empty($_GET['blog_categorias_id'])){
                $blog->where('blog_categorias_id',$_GET['blog_categorias_id']);
            }
            if(!empty($_GET['page'])){
                $blog->limit = array(($_GET['page']-1),6);
            }
            $blog->init('blog');
            foreach($this->blog->result() as $n=>$b){
                $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
                $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
            }
            
            $totalpages = round($this->db->get_where('blog')->num_rows()/6);
            $totalpages = $totalpages==0?'1':$totalpages;
            foreach($this->blog->result() as $n=>$b){
                $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            }
            if($this->blog->num_rows()>0){
                $this->blog->tags = $this->blog->row()->tags;
            }
            $this->loadView(
                    array(
                        'view'=>'frontend/main',
                        'detail'=>$this->blog,
                        'total_pages'=>$totalpages,
                        'current_page'=>!empty($_GET['page'])?$_GET['page']:1,
                        'title'=>'Blog',
                        'populares'=>$this->populares(),
                        'categorias'=>$this->get_categorias()
                    ));
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $blog = new Bdsource();
                $blog->where('id',$id);
                $blog->init('blog',TRUE);
                $this->blog->link = site_url('blog/'.toURL($this->blog->id.'-'.$this->blog->titulo));
                $this->blog->foto = base_url('img/blog/'.$this->blog->foto);
                $this->blog->categorias = $this->db->get_where('blog_categorias',array('id'=>$this->blog->blog_categorias_id));
                $this->blog->link = site_url('blog/'.toURL($id.'-'.$this->blog->titulo));
                if($blog->num_rows()>0){
                    $blog->visitas++;
                    $blog->save();
                }else{
                    throw new Exception("El contenido que desea visualizar, ya no existe o no se encuentra disponible",404);
                    die();
                }
                $comentarios = new Bdsource();
                $comentarios->where('blog_id',$this->blog->id);
                $comentarios->init('comentarios');
                $relacionados = new Bdsource();
                $relacionados->limit = array(4); 
                $relacionados->where('blog_categorias_id',$this->blog->blog_categorias_id);
                $relacionados->where('id !=',$this->blog->id);
                $relacionados->order_by = array('id','desc');
                $relacionados->init('blog',FALSE,'relacionados');
                foreach($this->relacionados->result() as $n=>$b){
                    $this->relacionados->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                    $this->relacionados->row($n)->foto = base_url('img/blog/'.$b->foto);
                    $this->relacionados->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
                }
                $blogs = $this->db->get('blog');
                $anterior = '';
                $siguiente = '';
                foreach($blogs->result() as $n=>$b){
                    if($b->id==$id){                        
                        $anterior = !empty($blogs->row($n-1)) && $n>0?$blogs->row($n-1):'';
                        $siguiente = !empty($blogs->row($n+1)) && $n<$blogs->num_rows()-1?$blogs->row($n+1):'';
                    }
                }
                
                if(!empty($anterior)){
                    $anterior->link = site_url('blog/'.toURL($anterior->id.'-'.$anterior->titulo));
                    $anterior->foto = base_url('img/blog/'.$anterior->foto);
                }
                if(!empty($siguiente)){
                    $siguiente->link = site_url('blog/'.toURL($siguiente->id.'-'.$siguiente->titulo));
                    $siguiente->foto = base_url('img/blog/'.$siguiente->foto);
                }
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->blog,
                        'title'=>$this->blog->titulo,
                        'comentarios'=>$this->comentarios,
                        'categorias'=>$this->get_categorias(),
                         'populares'=>$this->populares(),
                        'relacionados'=>$this->relacionados,
                        'anterior'=>$anterior,
                        'siguiente'=>$siguiente
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function comentarios(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                  ->set_rules('texto','Comentario','required')
                                  ->set_rules('blog_id','','required|numeric');
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $data['fecha'] = date("Y-m-d");
                $this->db->insert('comentarios',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('blog/'.$_POST['blog_id'].'#comentarios'));
            }else{
                $_SESSION['mensaje'] = $this->error('Comentario no enviado con éxito');
                header("Location:".base_url('blog/'.$_POST['blog_id'].'#comentarios'));                
            }
        }
    }
?>
