<section class="main-banner mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/blog.jpg" class="mv-banner-style-1 mv-bg-overlay-dark overlay-0-85 mv-parallax">
        <div class="page-name mv-caption-style-6">
            <div class="container">
                <div class="mv-title-style-9">
                    <span class="main">Noticias</span>
                    <img src="<?= base_url() ?>img/icon/icon_line_polygon_line.png" alt="icon" class="line"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- .main-banner-->

<section class="main-breadcrumb mv-wrap">
    <div class="mv-breadcrumb-style-1">
        <div class="container">
            <ul class="breadcrumb-1-list">
                <li>
                    <a href="<?= site_url() ?>">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><a href="<?= site_url('blog') ?>">Noticias</a></li>
                <li><a>Lista</a></li>
            </ul>
        </div>
    </div>
</section>
<!-- .main-breadcrumb-->

<section class="mv-main-body blog-mansory-3-no-sb-main mv-bg-gray mv-wrap">
    <div class="container">
        <div class="mv-list-blog-wrapper mv-block-style-15">
            <div class="row block-15-list mv-list-blog mv-post-mansory-wrapper">
                
                <?php foreach($detail->result() as $d): ?>
                    <article class="col-xs-6 col-sm-4 item item-blog-grid-3-mansory-no-sb mv-post-mansory-item post">
                        <div class="item-inner mv-box-shadow-gray-1">
                            <div class="item-inner-relative">
                                <div class="block-15-thumb mv-effect-relative">
                                    <?php if(!empty($d->foto)): ?>
                                        <a href="<?= $d->link ?>" title="<?= $d->titulo ?>">
                                            <span style="background-image: url(<?= $d->foto ?>);" class="block-15-thumb-img"></span>
                                        </a>
                                    <?php endif ?>                                    
                                    <span class="polygon hidden-xs hidden-sm"></span>
                                    <span class="block-15-date mv-date-style-1 hidden-xs hidden-sm">
                                        <span class="day"><?= strftime("%d",strtotime($d->fecha)); ?></span>
                                        <span class="month"><?= strftime("%b",strtotime($d->fecha)); ?></span>                                        
                                    </span>
                                </div>
                                <!-- .block-15-thumb-->
                                <div class="block-15-main">
                                    <div class="block-15-main-inner">
                                        <div class="block-15-title">
                                            <a href="<?= $d->link ?>" title="<?= $d->titulo ?>" class="mv-overflow-ellipsis">
                                                <?= $d->titulo ?>
                                            </a>
                                        </div>
                                        <ul class="block-15-meta mv-ul">
                                            <li class="meta-item mv-icon-left-style-3">
                                                <a href="<?= $d->link ?>">
                                                    <span class="text">
                                                        <span class="icon"><i class="fa fa-pencil-square-o"></i></span>
                                                        <?= $d->user ?>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="meta-item mv-icon-left-style-3">
                                                <a href="<?= $d->link ?>#comentarios">
                                                    <span class="text">
                                                        <span class="icon">
                                                            <i class="fa fa-comments-o"></i>
                                                        </span><?= $d->comentarios ?>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="meta-item mv-icon-left-style-3 hidden-md hidden-lg">
                                                <a href="#">
                                                    <span class="text">
                                                        <span class="icon">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                        <?= ucfirst(strftime("%b %d, %Y",strtotime($d->fecha))); ?>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="block-15-content"><?= substr(strip_tags($d->texto),0,255).'...' ?></div>
                                    </div>
                                </div>
                                <!-- .block-15-main-->
                            </div>
                        </div>
                    </article>
                    <!-- .post-->
                <?php endforeach ?>
                
            </div>
        </div>
        <!-- .mv-list-blog-wrapper-->

        <!--<div class="mv-pagination-wrapper">
            <div class="mv-pagination-style-1 text-center">
                <div class="pagination-loading"><img src="<?= base_url() ?>img/icon/icon_loading.gif" alt="icon"/>
                </div>
            </div>
            <!-- .mv-pagination-style-1
        </div>-->
        <!-- .mv-pagination-wrapper-->
    </div>
</section>
<!-- .mv-main-body-->
