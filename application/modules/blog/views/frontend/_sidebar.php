<div class="mv-c-s-sidebar col-xs-12 col-md-4 col-lg-3 hidden-xs hidden-sm">
    <div class="mv-c-s-sidebar-inner">
        <div class="mv-aside mv-aside-search">
            <div class="aside-title mv-title-style-11">Buscar...</div>
            <div class="aside-body">
                <form method="GET" class="form-aside-search" id="searchForm" action="<?= base_url('blog') ?>">
                    <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page'])?$_GET['page']:'1' ?>">
                    <input type="hidden" id="blog_categorias_id" name="blog_categorias_id" value="<?= !empty($_GET['blog_categorias_id'])?$_GET['blog_categorias_id']:'' ?>">
                    <div class="mv-inputbox-icon right">
                        <input type="search" name="direccion" class="mv-inputbox mv-inputbox-style-2"/>
                        <button type="submit" class="icon mv-btn mv-btn-style-4 fa fa-search"></button>
                    </div>
                </form>
            </div>
        </div>
        <!-- .mv-aside-search-->

        <div class="mv-aside mv-aside-category-blog">
            <div class="aside-title mv-title-style-11">Categorias</div>
            <div class="aside-body">
                <nav class="mv-menu-style-2">
                    <ul>
                        <?php if($categorias->num_rows()==0): ?>
                            <li>
                                <a href="#">Sin categorías</a>
                            </li>
                        <?php endif ?>
                        <?php foreach($categorias->result() as $c): ?>
                            <li>
                                <a href="javascript:changeCategoria(<?= $c->id ?>)" class="mv-col-wrapper">
                                    <span class="mv-col-left menu-text mv-icon-left-style-5"><?= $c->blog_categorias_nombre ?> </span>
                                    <span class="mv-col-right menu-number"><?= $c->cantidad ?></span>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- .mv-aside-category-blog-->

        <div class="mv-aside mv-aside-recent-posts">
            <div class="aside-title mv-title-style-11">Entradas Relacionadas</div>
            <div class="aside-body">
                <div class="recent-posts-list">
                    <div class="mv-block-style-24">
                        <div class="block-24-list">
                            
                            <?php foreach($relacionados->result() as $d): ?>
                                <article class="item item-aside-recent-posts post">
                                    <div class="item-inner">
                                        <div class="mv-dp-table">
                                            <div class="mv-dp-table-cell block-24-thumb">
                                                <div class="thumb-inner mv-lightbox-style-1">
                                                    <a href="<?= $d->link ?>" title="<?= $d->titulo ?>">
                                                        <span style="background-image: url(<?= $d->foto ?>);" class="block-24-thumb-img"></span>
                                                    </a>
                                                    <a href="<?= $d->foto ?>" data-lightbox-href="<?= $d->link ?>" title="<?= $d->titulo ?>" class="mv-btn mv-lightbox-item">
                                                        <i class="icon fa fa-share-square-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="mv-dp-table-cell block-24-main">
                                                <div class="block-24-title">
                                                    <a href="<?= $d->link ?>" title="<?= $d->titulo ?>"><?= $d->titulo ?></a>
                                                </div>
                                                <ul class="block-24-meta mv-ul">
                                                    <li class="meta-item mv-icon-left-style-3">
                                                        <a href="#">
                                                            <span class="text"><?= $d->user ?></span>
                                                        </a>
                                                    </li>
                                                    <li class="meta-item mv-icon-left-style-3">
                                                        <a href="<?= $d->link ?>#comentarios">
                                                            <span class="text"> 
                                                                <span class="icon"><i class="fa fa-comments-o"></i></span><?= $d->comentarios ?>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <!-- .post-->
                            <?php endforeach ?>
                            
                        </div>
                    </div>
                    <!-- .mv-block-style-24-->
                </div>
            </div>
        </div>
        <!-- .mv-aside-recent-posts-->

        <div class="mv-aside mv-aside-tags">
            <div class="aside-title mv-title-style-11">tags</div>
            <div class="aside-body">
                <div class="tag-list">
                    <div class="mv-btn-group">
                        <div class="group-inner">
                            <?php if(!empty($detail->tags)): ?>
                                <?php foreach(explode(',',$detail->tags) as $t): ?>
                                    <a href="<?= base_url('blog') ?>?direccion=<?= $t ?>" class="mv-btn mv-btn-style-22"><?= $t ?></a>
                                <?php endforeach ?>            
                            <?php endif ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .mv-aside-tags-->

        <div class="mv-aside mv-aside-most-view">
            <div class="aside-title mv-title-style-11">Populares</div>
            <div class="aside-body">
                <div class="most-view-list">
                    <div class="mv-block-style-29">
                        <div class="block-29-list">
                            
                            <?php foreach($relacionados->result() as $d): ?>
                                    <article class="item post">
                                        <div class="item-inner">
                                            <div class="mv-dp-table">
                                                <div class="mv-dp-table-cell block-29-date">
                                                    <div class="mv-date-style-2">
                                                        <div class="day"><?= strftime("%d",strtotime($d->fecha)); ?></div>
                                                        <div class="month"><?= strftime("%b",strtotime($d->fecha)); ?></div>
                                                    </div>
                                                </div>
                                                <div class="mv-dp-table-cell block-29-main">
                                                    <div class="block-29-title">
                                                        <a href="<?= $d->link ?>" title="<?= $d->titulo ?>"><?= $d->titulo ?></a>
                                                    </div>
                                                    <ul class="block-29-meta mv-ul">
                                                        <li class="meta-item mv-icon-left-style-3"><a href="#"><span class="text"><?= $d->user ?></span></a></li>
                                                        <li class="meta-item mv-icon-left-style-3"><a href="<?= $d->link ?>#comentarios"><span class="icon"><i class="fa fa-comments-o"></i></span><span class="text"><?= $d->comentarios ?></span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- .post-->
                            <?php endforeach ?>
                            
                            <!-- .post-->
                        </div>
                    </div>
                    <!-- .mv-block-style-29-->
                </div>
            </div>
        </div>
        <!-- .mv-aside-most-view-->
    </div>
</div>
<!-- .mv-c-s-sidebar-->
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchForm").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchForm").submit();
    }
</script>
