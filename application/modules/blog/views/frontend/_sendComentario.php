    <?php
    if (!empty($_SESSION['mensaje'])) {
        echo $_SESSION['mensaje'];
        unset($_SESSION['mensaje']);
    }
    ?>
    <div class="comment-respond">
        <div class="comment-respond-title mv-title-style-12">
            <span class="main">Deja tu comentario<span class="line"></span></span>
        </div>
        <div class="comment-respond-form">
            <form method="post" action="<?= base_url('blog/frontend/comentarios') ?>" class="form-respond mv-form-horizontal">
                <div class="mv-form-group">
                    <div class="col-xs-2 mv-label"> <strong class="text-uppercase">Nombre</strong></div>
                    <div class="col-xs-10 mv-field">
                        <input type="text" name="autor" class="mv-inputbox mv-inputbox-style-1 mv-inputbox-inline w-370"/>
                    </div>
                </div>

                <div class="mv-form-group">
                    <div class="col-xs-2 mv-label"> <strong class="text-uppercase">Email</strong></div>
                    <div class="col-xs-10 mv-field">
                        <input type="email" name="email" class="mv-inputbox mv-inputbox-style-1 mv-inputbox-inline w-370"/>
                    </div>
                </div>

                <div class="mv-form-group">
                    <div class="col-xs-2 mv-label"> <strong class="text-uppercase">Título</strong></div>
                    <div class="col-xs-10 mv-field">
                        <input type="text" name="titulo" class="mv-inputbox mv-inputbox-style-1 mv-inputbox-inline w-370"/>
                    </div>
                </div>

                <div class="mv-form-group input-message-wrapper">
                    <div class="col-xs-2 mv-label mv-p-0 mv-m-0"><strong class="text-uppercase"> </strong></div>
                    <div class="col-xs-10 mv-field">
                        <textarea name="texto" placeholder="Escribe un Comentario" data-mv-placeholder="Escribe un Comentario" class="mv-inputbox mv-inputbox-style-1"></textarea>
                    </div>
                </div>

                <div class="mv-form-group submit-button">
                    <div class="col-xs-2 mv-label mv-p-0 mv-m-0">
                        <strong class="text-uppercase"> </strong>
                    </div>
                    <div class="col-xs-10 mv-field">
                        <input name="blog_id" value="<?= $detail->id ?>" type="hidden">            
                        <button type="submit" class="mv-btn mv-btn-style-5">Enviar comentario</button>
                    </div>
                </div>
            </form>
            <!-- .form-respond-->
        </div>
    </div>
    <!-- .comment-respond-->