<div class="blog-detail-block block-comment" id='comentarios'>
    <div class="mv-comment-block-style-1 mv-box-shadow-gray-1">
        <div class="comment-header">
            <div class="comment-title mv-title-style-12">
                <span class="main">Existen <?= $comentarios->num_rows() ?> comentarios<span class="line"></span></span></div>
        </div>
        <!-- .comment-header-->

        <div class="comment-list">
            <ul class="comments-wrapper">
                <?php foreach($comentarios->result() as $c): ?>
                    <li id="comment_<?= $c->id ?>" class="comment">
                        <div class="comment-content-wrapper">
                            <div class="mv-dp-table">
                                <div class="mv-dp-table-cell content-thumb">
                                    <a href="#">
                                        <span style="background-image: url(<?= base_url() ?>img/avatar/avatar_100x100.png);" class="content-thumb-img"></span>
                                    </a>
                                </div>
                                <div class="mv-dp-table-cell content-main">
                                    <div class="content-meta">
                                        <ul>
                                            <li class="comment-author">
                                                <a href="#"><?= $c->autor; ?></a>
                                            </li>
                                            <li class="comment-date mv-icon-left-style-6">
                                                <?= date("Y-m-d",strtotime($c->fecha))."T00:00:00+00:00"; ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="content"><?= $c->texto ?></div>                                   
                                </div>
                            </div>
                        </div>
                        <!-- .comment-content-wrapper-->
                    </li>
                <?php endforeach ?>
                <!-- .comment-->                
            </ul>
        </div>
        <!-- .comment-list-->

        <?php $this->load->view('_sendComentario') ?>
        <!-- .comment-respond-->
    </div>
    <!-- .mv-comment-block-style-1-->
</div>