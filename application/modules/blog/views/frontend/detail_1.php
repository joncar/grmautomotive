<figure class="page-figure ajax-gallery-title blog-post-image">
  <img src="<?= $detail->foto ?>" alt="">
  <figcaption class="col-md-5">
    <h2><?= $detail->titulo ?></h2>
    <p class="post-info">
      <a href="#">
        <time><?= date("d-m-Y",strtotime($detail->fecha)) ?></time>
      </a>
      /
      <a href="#"><?= $detail->user ?></a>
    </p>
    <span class="post-tag">
      <?php if(!empty($detail->tags)): ?>
            <?php foreach(explode(',',$detail->tags) as $t): ?>
                <a href="#"><?= $t ?></a>/ 
            <?php endforeach ?>            
        <?php endif ?>
    </span>
  </figcaption>
</figure>

<div class="col-md-5 ajax-content-image">
  <?= $detail->texto ?>
</div>