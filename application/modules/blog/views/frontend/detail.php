<section class="main-banner mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/blog.jpg" class="mv-banner-style-1 mv-bg-overlay-dark overlay-0-85 mv-parallax">
        <div class="page-name mv-caption-style-6">
            <div class="container">
                <div class="mv-title-style-9">
                    <span class="main">Noticia</span>
                    <img src="<?= base_url() ?>img/icon/icon_line_polygon_line.png" alt="icon" class="line"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- .main-banner-->

<section class="main-breadcrumb mv-wrap">
    <div class="mv-breadcrumb-style-1">
        <div class="container">
            <ul class="breadcrumb-1-list">
                <li><a href="<?= site_url() ?>"><i class="fa fa-home"></i></a></li>
                <li><a href="<?= site_url('blog') ?>">Blog</a></li>
                <li><a><?= $detail->titulo ?></a></li>
            </ul>
        </div>
    </div>
</section>
<!-- .main-breadcrumb-->

<section class="mv-main-body blog-detail-main mv-bg-gray mv-wrap">
    <div class="container">
        <div class="row mv-content-sidebar">
            <div class="mv-c-s-content col-xs-12 col-md-8 col-lg-9">
                <div class="blog-detail-block block-main-content">
                    <div class="mv-block-style-30 mv-bg-white mv-box-shadow-gray-1">
                        <div class="block-30-header">
                            <div class="mv-dp-table">
                                <div class="mv-dp-table-cell block-30-date-wrapper hidden-xs">
                                    <div class="mv-date-style-1">
                                        <div class="day"><?= strftime("%d",strtotime($detail->fecha)); ?></div>
                                        <div class="month"><?= strftime("%b",strtotime($detail->fecha)); ?> </div>
                                    </div>
                                </div>
                                <div class="mv-dp-table-cell block-30-title-wrapper">
                                    <div class="block-30-col-inner">
                                        <div class="block-30-title"><?= $detail->titulo ?></div>
                                        <ul class="block-30-meta mv-ul">
                                            <li class="meta-item mv-icon-left-style-3">
                                                <a href="#">
                                                    <span class="text"> 
                                                        <span class="icon">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                        </span><?= $detail->user ?></span>
                                                </a>
                                            </li>
                                            <li class="meta-item mv-icon-left-style-3">
                                                <a href="#">
                                                    <span class="text"> 
                                                        <span class="icon">
                                                            <i class="fa fa-comments-o"></i>
                                                        </span>0 Comentarios</span>
                                                </a>
                                            </li>
                                            <li class="meta-item mv-icon-left-style-3 hidden-sm hidden-md hidden-lg">
                                                <a href="#">
                                                    <span class="text">
                                                        <span class="icon">
                                                            <i class="fa fa-calendar"></i>
                                                        </span><?= ucfirst(strftime("%b %d, %Y",strtotime($detail->fecha))); ?></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="block-30-post-format">
                                <i class="icon fa fa-file-image-o"></i>
                            </div>
                        </div>
                        <!-- .block-30-header-->

                        <div class="block-30-featured">
                            <div class="mv-block-style-40">
                                <div class="blog-detail-slider mv-slick-slide">
                                    <div class="block-40-gallery-main">
                                        <div class="slider gallery-main">
                                            
                                            <div class="slick-slide">
                                                <div class="slick-slide-inner">
                                                    <img src="<?= $detail->foto ?>" alt="<?= $detail->titulo ?>" class="block-40-main-img"/>
                                                </div>
                                            </div>
                                            <!-- .slick-slide-->
                                            
                                        </div>
                                    </div>
                                    <!-- .block-40-gallery-main-->

                                    <div class="block-40-gallery-thumbs">
                                        <div class="gallery-thumbs-wrapper">
                                            <div class="slider gallery-thumbs">
                                                <div class="slick-slide">
                                                    <div class="slick-slide-inner">
                                                        <img src="<?= $detail->foto ?>" alt="<?= $detail->titulo ?>" class="block-40-thumbs-img"/>
                                                        <span class="btn-zoom"><i class="icon fa fa-search"></i></span>
                                                    </div>
                                                </div>
                                                <!-- .slick-slide-->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .block-40-gallery-thumbs-->
                                </div>
                                <!-- .blog-detail-slider-->
                            </div>
                            <!-- .mv-block-style-40-->
                        </div>
                        <!-- .block-30-featured-->

                        <div class="block-30-main">
                            <div class="block-30-main-inner">
                                <div class="block-30-content clearfix">
                                    <?= $detail->texto ?>
                                </div>
                                <!-- .block-30-quote-->

                                <div class="block-30-footer">
                                    <div class="mv-dp-table align-top">
                                        <div class="mv-dp-table-cell block-30-tag">
                                            <div class="mv-btn-group">
                                                <div class="group-inner block-30-tag-list">
                                                    <span class="mv-btn block-30-footer-title">
                                                        <i class="fa fa-share-alt"></i>&nbsp; Tags:</span>
                                                        <?php if(!empty($detail->tags)): ?>
                                                            <?php foreach(explode(',',$detail->tags) as $t): ?>
                                                                <a href="<?= base_url('blog') ?>?direccion=<?= $t ?>" class="mv-btn mv-btn-style-22"><?= $t ?></a>
                                                            <?php endforeach ?>            
                                                        <?php endif ?>                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mv-dp-table-cell block-30-social text-right">
                                            <div class="mv-social-list-style-1">
                                                <ul class="social-ul mv-ul clearfix">
                                                    <li class="block-30-footer-title"><i class="fa fa-share-alt"></i>&nbsp; Compartir:</li>
                                                    <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                    <li><a href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- .block-30-footer-->
                            </div>
                        </div>
                    </div>
                    <!-- .mv-block-style-30-->
                </div>
                <!-- .block-main-content-->

                <div class="blog-detail-block block-post-pagination">
                    <nav class="mv-block-style-31 mv-box-shadow-gray-1">
                        <div class="mv-dp-table align-middle">
                            <?php if(!empty($anterior)): ?>
                                <div class="mv-dp-table-cell block-31-left">
                                    <div class="block-31-title-wrapper">
                                        <div class="block-31-title">
                                            <a href="<?= $anterior->link ?>" title="<?= $anterior->titulo ?>">
                                                <?= $anterior->titulo ?>
                                            </a>
                                        </div>
                                        <div class="block-31-author"><?= $anterior->user ?></div>
                                    </div>
                                    <a href="<?= $anterior->link ?>" title="<?= $anterior->titulo ?>" class="mv-btn mv-btn-style-9 block-31-button">
                                        <span class="btn-inner">
                                            <span class="btn-behind">Ant</span>
                                            <span class="btn-icon fa fa-angle-double-left"></span>                                            
                                        </span>
                                    </a>
                                </div>
                            <?php endif ?>
                            <?php if(!empty($siguiente)): ?>
                                <div class="mv-dp-table-cell block-31-right">
                                    <div class="block-31-title-wrapper">
                                        <div class="block-31-title">
                                            <a href="<?= $siguiente->link ?>" title="<?= $siguiente->titulo ?>">
                                                <?= $siguiente->titulo ?>
                                            </a>
                                        </div>
                                        <div class="block-31-author"><?= $siguiente->user ?></div>
                                    </div>
                                    <a href="<?= $siguiente->link ?>" title="<?= $siguiente->titulo ?>" class="mv-btn mv-btn-style-9 block-31-button">
                                        <span class="btn-inner">
                                            <span class="btn-behind">Sig</span>
                                            <span class="btn-icon fa fa-angle-double-right"></span>
                                        </span>
                                    </a>
                                </div>
                            <?php endif ?>
                        </div>
                    </nav>
                    <!-- .mv-block-style-31-->
                </div>
                <!-- .block-post-pagination-->
<!--                --><?php //$this->load->view('frontend/_comentarios',array('comentarios'=>$comentarios)); ?>

                <!-- .block-comment-->
            </div>
            <!-- .mv-c-s-content-->

            <?php $this->load->view('frontend/_sidebar'); ?>
        </div>
    </div>
</section>
<!-- .mv-main-body-->