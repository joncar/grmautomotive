<section class="checkout-main mv-bg-gray mv-wrap" style="padding-bottom: 50px; ">
    <div class="container">
        <div class="checkout-block block-billing-address mv-mb-50">
            <div class="mv-form-style-2 mv-box-shadow-gray-1">
                <div class="row">
                    <div class="col-sm-6 col-billing-address">
                        <div class="form-billing-address">
                            <div class="form-header">
                                <div class="mv-title-style-13">
                                    <div class="text-main">Información</div>
                                </div>
                            </div>
                            <!-- .form-header-->

                            <div class="form-body">                                

                                <div class="mv-form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="mv-label"> 
                                                <strong class="text-uppercase">Nombres <span class="mv-color-primary">*</span></strong>
                                            </div>
                                            <div class="mv-field">
                                                <input type="text" name="test127" class="mv-inputbox mv-inputbox-style-2"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="mv-label"> 
                                                <strong class="text-uppercase">Apellidos <span class="mv-color-primary">*</span></strong>
                                            </div>
                                            <div class="mv-field">
                                                <input type="text" name="test127" class="mv-inputbox mv-inputbox-style-2"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mv-form-group">
                                    <div class="mv-label"> <strong class="text-uppercase">Email</strong></div>
                                    <div class="mv-field">
                                        <input type="text" name="test127" class="mv-inputbox mv-inputbox-style-2"/>
                                    </div>
                                </div>

                                <div class="mv-form-group">
                                    <div class="mv-label"> 
                                        <strong class="text-uppercase">Teléfono <span class="mv-color-primary">*</span></strong>
                                    </div>
                                    <div class="mv-field">
                                        <div class="mv-inputbox-list">
                                            <input type="text" name="test127" placeholder="" data-mv-placeholder="" class="mv-inputbox mv-inputbox-style-2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- .form-body-->
                        </div>
                        <!-- .form-billing-address-->
                    </div>

                    <div class="col-sm-6 col-shipping-address">
                        <div class="form-different-address">
                            

                            <div class="form-body">                                                                

                                <div class="mv-form-group">
                                    <div class="mv-label"> 
                                        <strong class="text-uppercase">Comentarios</strong>
                                    </div>
                                    <div class="mv-field">
                                        <textarea name="test127" rows="5" placeholder="Algún comentario sobre su presupuesto, duda o pregunta" data-mv-placeholder="Algún comentario sobre su presupuesto, duda o pregunta" class="mv-inputbox mv-inputbox-style-2 mv-resize-vertical"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- .form-body-->
                        </div>
                        <!-- .form-shipping-address-->
                    </div>
                </div>
            </div>
        </div>

        <div class="checkout-block block-button-place-order mv-box-shadow-gray-1">
            <div class="mv-dp-table align-middle">                
                <div class="mv-dp-table-cell col-button text-right">
                    <a href="#" class="mv-btn mv-btn-style-5 btn-5-h-45 responsive-btn-5-type-1">Enviar solicitud</a>
                </div>
            </div>
        </div>
        <!-- .block-button-place-order-->
    </div>
</section>
<!-- .mv-main-body-->
