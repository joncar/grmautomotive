<section class="main-banner mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/demo_bg_1920x1680.png" class="mv-banner-style-1 mv-bg-overlay-dark overlay-0-85 mv-parallax">
        <div class="page-name mv-caption-style-6">
            <div class="container">
                <div class="mv-title-style-9"><span class="main">Mi Presupuesto</span><img src="<?= base_url() ?>img/icon/icon_line_polygon_line.png" alt="icon" class="line"/></div>
            </div>
        </div>
    </div>
</section>
<!-- .main-banner-->

<section class="main-breadcrumb mv-wrap">
    <div class="mv-breadcrumb-style-1">
        <div class="container">
            <ul class="breadcrumb-1-list">
                <li><a href="<?= site_url() ?>"><i class="fa fa-home"></i></a></li>
                <li><a>Mi Presupuesto</a></li>
            </ul>
        </div>
    </div>
</section>
<!-- .main-breadcrumb-->

<section class="mv-main-body wishlist-main mv-bg-gray mv-wrap">
    <div class="container">
        <?= $output ?>
    </div>
</section>
<?php $this->load->view('frontend/checkout') ?>

