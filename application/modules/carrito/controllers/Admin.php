<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function presupuesto(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Presupuesto');
            $crud->set_relation('productos_id','productos',"{productos_nombre},{descripcion},{foto_portada},{tipo},{tipo_llanta},{material}");                
            $crud->columns('foto','producto','cantidad','precio','tipo','tipo_llanta','descripcion','link','material');
            $crud->callback_column('foto',function($val,$row){
                return base_url().'img/productos/'.trim(explode(',',$row->s4b04c546)[2]);
            });
            $crud->callback_column('producto',function($val,$row){
                return explode(',',$row->s4b04c546)[0];
            });
            $crud->callback_column('tipo',function($val,$row){
                return explode(',',$row->s4b04c546)[3];
            });
            $crud->callback_column('material',function($val,$row){
                return explode(',',$row->s4b04c546)[5];
            });
            $crud->callback_column('tipo_llanta',function($val,$row){
                return explode(',',$row->s4b04c546)[4];
            });
            $crud->callback_column('descripcion',function($val,$row){
                return explode(',',$row->s4b04c546)[1];
            });
            $crud->callback_column('link',function($val,$row){
                return site_url('producto/'.toURL($row->productos_id.'-'.explode(',',$row->s4b04c546)[0]));
            });
            $crud->unset_add();
            $output = $crud->render();
            $output->title = 'Solicitudes de Presupuesto';
            $this->loadView($output);
        }
    }
?>
