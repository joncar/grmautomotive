<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        public function presupuesto(){  
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');                
            $crud = new ajax_grocery_CRUD();
            $usuario = empty($_SESSION['user'])?$_SESSION['visitaid']:$_SESSION['user'];
            $crud->where('usuario',$usuario); 
            $crud->set_subject('Presupuesto')
                 ->set_table('presupuesto')
                 ->set_theme('crud')
                 ->set_url('carrito/frontend/presupuesto/');
            $crud->required_fields('productos_id');
            $crud->set_relation('productos_id','productos',"{productos_nombre},{descripcion},{foto_portada},{disponible},{referencia}");                
            $crud->columns('productoss_id','foto','producto','cantidad','precio','descripcion','link','disponibilidad','disponible','referencia');
            $crud->callback_column('foto',function($val,$row){
                return base_url().'img/productos/'.trim(explode(',',$row->s4b04c546)[2]);
            });
            $crud->callback_column('producto',function($val,$row){
                return explode(',',$row->s4b04c546)[0];
            });
            $crud->callback_column('productoss_id',function($val,$row){
                return $row->productos_id;
            });
            $crud->callback_column('disponible',function($val,$row){
                $disponible = explode(',',$row->s4b04c546)[3];
                return $disponible==1?'<div class="mv-btn mv-btn-style-6 btn-6-instock">En Stock</div>':'<div class="mv-btn mv-btn-style-6 btn-6-out-of-stock">Agotado</div>';
            });
            $crud->callback_column('disponibilidad',function($val,$row){
                return explode(',',$row->s4b04c546)[3];
            });
            
            $crud->callback_column('referencia',function($val,$row){
                return explode(',',$row->s4b04c546)[4];
            });
            
            $crud->callback_column('descripcion',function($val,$row){
                return explode(',',$row->s4b04c546)[1];
            });
            $crud->callback_column('link',function($val,$row){
                return site_url('producto/'.toURL($row->productos_id.'-'.explode(',',$row->s4b04c546)[0]));
            });
            $crud->callback_column('cantidad',function($val,$row){
                return '<input type="text" name="cantidad" value="'.$val.'" class="mv-inputbox mv-only-numeric input-quantity-cart cantidad" data-pre="'.$row->id.'" data-id="'.$row->productos_id.'"/>';
            });
            $crud->callback_before_insert(function($post){
                $post['usuario'] = empty($_SESSION['user'])?$_SESSION['visitaid']:$_SESSION['user'];
                $post['cantidad'] = 1;
                if($this->db->get_where('presupuesto',$post)->num_rows()==0){
                    return $post;
                }else{
                    return FALSE;
                }
            });
            $crud->unset_jquery();
            if(!empty($_GET)){
                foreach($_GET as $n=>$v){
                    $crud->like($n,$v); 
                }
            }
            $accion = $crud->getParameters();
            $output = $crud->render('','application/modules/carrito/views/');
            $output->view = 'frontend/main';
            $output->scripts = get_header_crud($output->css_files, $output->js_files,TRUE); 
            $output->title = 'Solicitar Presupuesto';
            $this->loadView($output); 
        }
        
        public function checkout(){
            $this->loadView('frontend/checkout');
        }
    }
?>
