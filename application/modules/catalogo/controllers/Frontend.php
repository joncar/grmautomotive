<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        function get_categorias(){
            $this->db->limit('8');
            $categorias = $this->db->get_where('blog_categorias');
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('blog',array('blog_categorias_id'=>$c->id))->num_rows();
            }
            return $categorias;
        }                
        
        public function populares(){
            $blog = new Bdsource();
            $blog->limit = array('2','0');
            $blog->init('blog',FALSE,'populares');
            foreach($this->populares->result() as $n=>$b){
                $this->populares->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                $this->populares->row($n)->foto = base_url('img/blog/'.$b->foto);
            }
            foreach($this->populares->result() as $n=>$b){
                $this->populares->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows;                
            }
            return $this->populares;
        }
        
        public function index(){
            $blog = new Bdsource();
            $blog->limit = array('6','0');
            $blog->order_by = array('visitas','DESC');
            if(!empty($_GET['direccion'])){
                $blog->like('titulo',$_GET['direccion']);
            }
            if(!empty($_GET['blog_categorias_id'])){
                $blog->where('blog_categorias_id',$_GET['blog_categorias_id']);
            }
            if(!empty($_GET['page'])){
                $blog->limit = array(($_GET['page']-1),6);
            }
            $blog->init('blog');
            foreach($this->blog->result() as $n=>$b){
                $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
                $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
                $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
            }
            
            $totalpages = round($this->db->get_where('blog')->num_rows()/6);
            $totalpages = $totalpages==0?'1':$totalpages;
            foreach($this->blog->result() as $n=>$b){
                $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            }
            if($this->blog->num_rows()>0){
                $this->blog->tags = $this->blog->row()->tags;
            }
            $this->loadView(
                    array(
                        'view'=>'frontend/main',
                        'detail'=>$this->blog,
                        'total_pages'=>$totalpages,
                        'current_page'=>!empty($_GET['page'])?$_GET['page']:1,
                        'title'=>'Catalogo',
                        'populares'=>$this->populares(),
                        'categorias'=>$this->get_categorias()
                    ));
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $productos = new Bdsource();
                $productos->where('id',$id);
                $productos->init('productos',TRUE);
                $this->productos->link = site_url('productos/'.toURL($this->productos->id.'-'.$this->productos->productos_nombre));
                $this->productos->foto_portada = base_url('img/productos/'.$this->productos->foto_portada);
                $this->productos->foto_hover = base_url('img/productos/'.$this->productos->foto_hover);
                $this->productos->categorias = $this->db->get_where('categoria_productos',array('id'=>$this->productos->categoria_productos_id));                
                
                $relacionados = new Bdsource();
                $relacionados->limit = array(4); 
                $relacionados->where('categoria_productos_id',$this->productos->categoria_productos_id);
                $relacionados->where('id !=',$this->productos->id);
                $relacionados->order_by = array('id','desc');
                $relacionados->init('productos',FALSE,'relacionados');
                foreach($this->relacionados->result() as $n=>$b){
                    $this->relacionados->row($n)->link = site_url('producto/'.toURL($b->id.'-'.$b->productos_nombre));
                    $this->relacionados->row($n)->foto_portada = base_url('img/productos/'.$b->foto_portada);
                    $this->relacionados->row($n)->foto_hover = base_url('img/productos/'.$b->foto_hover);
                }
                
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->productos, 
                        'relacionados'=>$this->relacionados,
                        'title'=>$this->productos->productos_nombre
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function listado($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');                
                $crud = new ajax_grocery_CRUD();
                $crud->set_subject('Catalogo')
                     ->set_table('productos')
                     ->set_theme('crud')
                     ->set_url('catalogo/frontend/listado/'.$id.'/'); 
                $crud->columns('oferta','descuento','referencia','productos_nombre','foto_portada','foto_hover','link');
                $crud->callback_column('foto_portada',function($val,$row){return base_url('img/productos/'.$val);})
                     ->callback_column('foto_hover',function($val,$row){return base_url('img/productos/'.$val);})
                     ->callback_column('link',function($val,$row){return site_url('producto/'.$row->id.'-'.$row->productos_nombre);});
                $crud->set_primary_key('productos_id','caracteristicas');
                $crud->set_relation('id','caracteristicas','nombre');
                $crud->unset_jquery();   
                $crud->group_by('productos_id');
                $crud->where('categoria_productos_id',$id);
                if(!empty($_GET['namesearch']) && !empty($_GET['valuesearch'])){
                    $crud->like('jb80bb774.nombre',$_GET['namesearch']);
                    $crud->like('jb80bb774.valor',$_GET['valuesearch']);                    
                }elseif(!empty($_POST['namesearch']) && !empty($_POST['valuesearch'])){
                    $crud->like('jb80bb774.nombre',$_POST['namesearch']);
                    $crud->like('jb80bb774.valor',$_POST['valuesearch']);                    
                }elseif(!empty($_GET)){
                    foreach($_GET as $n=>$g){
                        $crud->like($n,$g);
                    }
                }
                
                $output = $crud->render('','application/modules/catalogo/views/');
                
                $output->title = @$this->db->get_where('productos',array('id'=>$id))->row()->productos_nombre;
                $output->view = 'frontend/main';
                $output->scripts = get_header_crud($output->css_files, $output->js_files,TRUE);
                $output->categoria = $id;
                $this->loadView($output);
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function compare($x = ''){
             if(empty($x)){
               $this->db->where('productos.id',-1);
            }
            $x = explode('-',$x);
            foreach($x as $y){
                if(is_numeric($y)){
                    $this->db->or_where('productos.id',$y);
                }
            }
           
            $productos = $this->db->get('productos');
            foreach($productos->result() as $n=>$b){
                $productos->row($n)->link = site_url('producto/'.toURL($b->id.'-'.$b->productos_nombre));
                $productos->row($n)->foto_portada = base_url('img/productos/'.$b->foto_portada);
                $productos->row($n)->foto_hover = base_url('img/productos/'.$b->foto_hover);
            }            
            $this->loadView(
                array(
                    'productos'=>$productos,
                    'view'=>'frontend/compare',
                    'title'=>'Comparar productos'
                ));
        }
        
        public function wishlist(){  
            if(!empty($_SESSION['user'])){
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');                
                $crud = new ajax_grocery_CRUD();
                $crud->set_subject('WishList')
                     ->set_table('wishlist')
                     ->set_theme('wishlist')
                     ->set_url('catalogo/frontend/wishlist/');
                $crud->required_fields('productos_id');
                $crud->set_relation('productos_id','productos',"{productos_nombre},{descripcion},{foto_portada},{disponible},{referencia}");                
            $crud->columns('productoss_id','foto','producto','cantidad','precio','descripcion','link','disponibilidad','disponible','referencia');
            $crud->callback_column('foto',function($val,$row){
                return base_url().'img/productos/'.trim(explode(',',$row->s4b04c546)[2]);
            });
            $crud->callback_column('producto',function($val,$row){
                return explode(',',$row->s4b04c546)[0];
            });
            $crud->callback_column('productoss_id',function($val,$row){
                return $row->productos_id;
            });
            $crud->callback_column('disponible',function($val,$row){
                $disponible = explode(',',$row->s4b04c546)[3];
                return $disponible==1?'<div class="mv-btn mv-btn-style-6 btn-6-instock">En Stock</div>':'<div class="mv-btn mv-btn-style-6 btn-6-out-of-stock">Agotado</div>';
            });
            $crud->callback_column('disponibilidad',function($val,$row){
                return explode(',',$row->s4b04c546)[3];
            });
            
            $crud->callback_column('referencia',function($val,$row){
                return explode(',',$row->s4b04c546)[4];
            });
            
            $crud->callback_column('descripcion',function($val,$row){
                return explode(',',$row->s4b04c546)[1];
            });
            $crud->callback_column('link',function($val,$row){
                return site_url('producto/'.toURL($row->productos_id.'-'.explode(',',$row->s4b04c546)[0]));
            });
            $crud->callback_column('cantidad',function($val,$row){
                return '<input type="text" name="cantidad" value="'.$val.'" class="mv-inputbox mv-only-numeric input-quantity-cart cantidad" data-pre="'.$row->id.'" data-id="'.$row->productos_id.'"/>';
            });
                $crud->callback_before_insert(function($post){
                    $post['user_id'] = $_SESSION['user'];
                    if($this->db->get_where('wishlist',$post)->num_rows()==0){
                        return $post;
                    }else{
                        return FALSE;
                    }
                });
                $crud->unset_jquery();
                $crud->where('user_id',$this->user->id);
                if(!empty($_GET)){
                    foreach($_GET as $n=>$v){
                        $crud->like($n,$v); 
                    }
                }
                $accion = $crud->getParameters();
                $output = $crud->render('','application/modules/catalogo/views/');
                $output->view = 'frontend/wishlist';
                $output->scripts = get_header_crud($output->css_files, $output->js_files,TRUE);                
                $output->title = 'Mi lista de preferencias';
                $this->loadView($output); 
            }else{
                redirect('registro/index/add');
            }
        }
        
        public function comentarios(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                  ->set_rules('texto','Comentario','required')
                                  ->set_rules('blog_id','','required|numeric');
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $data['fecha'] = date("Y-m-d");
                $this->db->insert('comentarios',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('blog/'.$_POST['blog_id'].'#respond'));
            }else{
                $_SESSION['mensaje'] = $this->error('Comentario no enviado con éxito');
                header("Location:".base_url('blog/'.$_POST['blog_id'].'#respond'));                
            }
        }
    }
?>
