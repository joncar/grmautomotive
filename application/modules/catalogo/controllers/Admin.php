<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function categoria_productos(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/productos');
            $crud->columns('categoria_nombre','foto','descripcion','productos añadidos');
            $crud->callback_column('productos añadidos',function($val,$row){
                return '<a href="'.base_url('catalogo/admin/productos/'.$row->id).'">'.get_instance()->db->get_where('productos',array('categoria_productos_id'=>$row->id))->num_rows().' <i class="fa fa-plus"></i></a>';
            });
            $crud->field_type('caracteristicas','tags');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function productos($x = '',$y = '',$z = ''){
            $crud = $this->crud_function('','');
            $this->categoria = $x;
            if(is_numeric($z)){
                $this->id = $z;
            }
            $crud->where('categoria_productos_id',$x);
            $crud->field_type('categoria_productos_id','hidden',$x)
                 ->field_type('oferta','checkbox')
                 ->field_type('disponible','checkbox')
                 ->field_type('tags','tags');
            $crud->order_by('posicion_main','ASC');
            $crud->columns('foto_portada','productos_nombre','disponible','oferta','referencia','posicion_main');
            if($crud->getParameters(FALSE)=='add' || $crud->getParameters(FALSE)=='edit'){
                $crud->fields('categoria_productos_id','productos_nombre','descripcion_corta','descripcion','referencia','disponible','foto_portada','foto_hover','oferta','descuento','tags','caracteristicas');
            }
            $crud->callback_after_insert(function($post,$primary){
                //Guardar caracteristicas
                if(!empty($_POST['caracteristicas']) && count($_POST['caracteristicas'])>0){
                    foreach($_POST['caracteristicas'] as $n=>$v){
                        get_instance()->db->insert('caracteristicas',array('nombre'=>$_POST['caractname'][$n],'valor'=>$v,'productos_id'=>$primary));
                    }
                }
            });
            $crud->callback_after_update(function($post,$primary){
                //Guardar caracteristicas
                get_instance()->db->delete('caracteristicas',array('productos_id'=>$primary));
                if(!empty($_POST['caracteristicas']) && count($_POST['caracteristicas'])>0){                    
                    foreach($_POST['caracteristicas'] as $n=>$v){
                        get_instance()->db->insert('caracteristicas',array('nombre'=>$_POST['caractname'][$n],'valor'=>$v,'productos_id'=>$primary));
                    }
                }
            });
            $crud->callback_column('posicion_main',function($val,$row){
                return form_dropdown("posicion_main",array('99'=>'Seleccione una posición','1'=>'1','2'=>'2','3'=>'3'),$val,' data-id="'.$row->id.'" class="form-control posicionmain"');
            });
            $crud->callback_field('caracteristicas',function($val){
                $categoria = get_instance()->db->get_where('categoria_productos',array('id'=>$this->categoria));
                $str = 'Sin caracteristicas para añadir';
                if($categoria->num_rows()){
                    $str = '<table class="table">';
                    $str.= '<tr><th>Caracteristica</th><th>Valor</th></tr>';
                    foreach(explode(",",$categoria->row()->caracteristicas) as $c){
                        $value = "";
                        if(!empty($this->id) && is_numeric($this->id)){
                            $value = $this->db->get_where('caracteristicas',array('nombre'=>$c,'productos_id'=>$this->id));
                            $value = $value->num_rows()>0?$value->row()->valor:'';
                        }
                        $str.= '<tr><td>'.$c.'</td><td><input type="hidden" name="caractname[]" value="'.$c.'"><input type="text" name="caracteristicas[]" class="form-control" value="'.$value.'"></td></tr>';
                    }
                    $str.= '</table>';
                }
                return $str;
            });
            $crud->set_lang_string('insert_success_message','Producto guardado con éxito, <a href="'.base_url('catalogo/admin/productos_fotos').'/{id}">Subir más fotos</a> |');
            $crud->unset_columns('categoria_productos_id');
            $crud->add_action('<i class="fa fa-image"></i> Adm. Fotos','',base_url('catalogo/admin/productos_fotos').'/');
            $crud->set_field_upload('foto_portada','img/productos');
            $crud->set_field_upload('foto_hover','img/productos');
            $crud = $crud->render();
            $crud->output = $this->load->view('crud/productos',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }
        
        function cambiar_posicion($id,$pos){
            $mostrar = $pos!=99?'1':'0';
            $this->db->update("productos",array("posicion_main"=>$pos,'mostrar_en_main'=>$mostrar),array("id"=>$id));
        }
        
        public function productos_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_ordering_field('priority')
                 ->set_table('productos_fotos')
                 ->set_image_path('img/productos')
                 ->set_url_field('url')
                 ->set_relation_field('productos_id');
            $crud->module = "catalogo";
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
