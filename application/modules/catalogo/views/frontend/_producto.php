
<div class="item-inner mv-effect-translate-top-bottom">
    <div class="content-thumb" style="background:<?= empty($color)?'':$color ?>">
        <div class="thumb-inner mv-effect-relative">
            <a href="<?= $p->link ?>" title="<?= $p->productos_nombre ?>">
                <img src="<?= $p->foto_portada ?>" alt="<?= $p->productos_nombre ?>" class="mv-effect-item active">
                <img src="<?= $p->foto_hover ?>" alt="<?= $p->productos_nombre ?>" class="mv-effect-item">                
            </a>

            <div class="content-sale-off mv-label-style-2 text-center">
                <div class="label-2-inner">
                    <ul class="label-2-ul">
                        <li class="number">REF: </li>
                        <li class="text"><?= $p->referencia ?></li>

                    </ul>
                </div>
            </div>

            <div class="patternproduct"></div>
            <div class="content-message mv-message-style-1">
                <div class="message-inner"></div>
            </div>
            
        </div>

        <div class="content-hover">
            <div class="content-button mv-btn-group text-center">
                <div class="group-inner">
                    <button class="mv-btn mv-btn-style-3 responsive-btn-3-type-1 btn-add-to-wishlist" data-id="<?= empty($_SESSION['user'])?0:$p->id ?>" title="Añadir a mis preferencias" type="button"><i class="fa fa-heart-o"></i></button>
                    <button class="mv-btn mv-btn-style-3 responsive-btn-3-type-1 btn-compare" data-id='<?=  $p->id ?>' title="Comparar" type="button"><i class="fa fa-signal"></i></button>
                    <button class="mv-btn mv-btn-style-3 responsive-btn-3-type-1 btn-add-to-cart" data-id='<?= $p->id ?>' title="Añadir a mi presupuesto" type="button" <?= $p->disponible==0?'disabled="true"':'' ?>><i class="fa fa-hand-o-right"></i></button>
                </div>
            </div>
        </div>
        <?php if(!empty($p->descuento)): ?>
            <div class="content-sale-off mv-label-style-3 label-dark"> 
                <div class="label-inner"><?= $p->descuento ?>%</div>
            </div>
        <?php endif ?>
    </div>

    <div class="content-default">
        <div data-rate="true" data-score="5.0" class="content-rate mv-rate text-center">
            <div class="rate-inner mv-f-14 text-left">
                <div class="content-price">
                    <span class="new-price" ><?= $p->productos_nombre ?></span>
                </div>
                <div class="content-desc">
                    <?= $p->descripcion_corta ?>
                </div>
                <div class="mv-title-style-3 color-dark">
                    <div class="title-3-text">  </div>
                    <div class="title-3-line"></div>
                </div>
            </div>
        </div>
        <div class="content-price" style="height:54px">
            <span class="new-price" style="font-weight:inherit"></span><br/>
            <span class="new-price" style="font-weight:inherit"></span>
        </div>
        <div class="content-desc">                
            <a href="<?= $p->link ?>" title="<?= $p->productos_nombre ?>" class="mv-overflow-ellipsis"></a>
        </div>
    </div>
</div>
