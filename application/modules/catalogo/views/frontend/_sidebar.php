<div class="mv-c-s-sidebar col-xs-12 col-md-4 col-lg-3 hidden-xs hidden-sm">
    <div class="mv-c-s-sidebar-inner">
        <div class="mv-aside mv-aside-search">
            <div class="aside-title mv-title-style-11">Buscar</div>
            <div class="aside-body">
                <div class="mv-inputbox-icon right">
                    <form method="get" id="filtering_form" action="">
                        <input type="text" id="field-productos_nombre" name="productos_nombre" class="mv-inputbox mv-inputbox-style-2" value="<?= !empty($_GET['productos_nombre'])?$_GET['productos_nombre']:'' ?>"/>
                        <input type="hidden" id="namesearch" name="namesearch" value="">
                        <input type="hidden" id="valuesearch" name="valuesearch" value="">
                        <button type="submit"  id="buscarproducto" class="icon mv-btn mv-btn-style-4 fa fa-search"></button>
                    </form>
                </div>
            </div>
        </div>
        <!-- .mv-aside-search-->         
        <?php 
            $this->db->select('caracteristicas.*');
            $this->db->join('productos','productos.id = caracteristicas.productos_id');
            $this->db->join('categoria_productos','categoria_productos.id = productos.categoria_productos_id');            
            $this->db->group_by('caracteristicas.nombre'); 
        ?>        
        <?php foreach($this->db->get('caracteristicas')->result() as $t): ?>
            <div class="mv-aside mv-aside-product-type">
                <div class="aside-title mv-title-style-11"><?= $t->nombre ?></div>
                <div class="aside-body">
                    <nav class="product-type-menu mv-menu-style-1">
                        <ul>
                            <?php 
                                $this->db->select('caracteristicas.*, COUNT(productos_id) as cantidad'); 
                                $this->db->group_by('caracteristicas.valor');
                             ?>
                            <?php foreach($this->db->get_where('caracteristicas',array('nombre'=>$t->nombre))->result() as $p): ?>
                                <?php if(!empty($p->valor)): ?>
                                    <li><a href="javascript:changevalue('<?= trim($t->nombre) ?>','<?= $p->valor ?>')" class="mv-icon-left-style-5"><?= $p->valor ?><span class="sub-text">&nbsp; (<?= $p->cantidad ?>)</span></a></li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- .mv-aside-product-type-->
        <?php endforeach ?>
        
        
    </div>
</div>
<!-- .mv-c-s-sidebar-->
<script>
    function changevalue(id,value){
        $("#namesearch").val(id);
        $("#valuesearch").val(value);
        $("#buscarproducto").trigger('click');
    }        
</script>
