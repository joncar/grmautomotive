<section class="main-banner mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/demo_bg_1920x1680.png" class="mv-banner-style-1 mv-bg-overlay-dark overlay-0-85 mv-parallax">
        <div class="page-name mv-caption-style-6">
            <div class="container">
                <div class="mv-title-style-9"><span class="main">Detalle de producto</span><img src="<?= base_url() ?>img/icon/icon_line_polygon_line.png" alt="icon" class="line"/></div>
            </div>
        </div>
    </div>
</section>
<!-- .main-banner-->

<section class="main-breadcrumb mv-wrap">
    <div class="mv-breadcrumb-style-1">
        <div class="container">
            <ul class="breadcrumb-1-list">
                <li><a href="<?= site_url() ?>"><i class="fa fa-home"></i></a></li>
                <li><a><?= $detail->productos_nombre ?></a></li>
            </ul>
        </div>
    </div>
</section>
<!-- .main-breadcrumb-->

<section class="mv-main-body product-detail-main mv-bg-gray mv-wrap">
    <div class="container">
        <div class="post">
            <div class="block-info mv-box-shadow-gray-1">
                <div class="mv-block-style-27">
                    <div class="mv-col-wrapper">
                        <div class="mv-col-left block-27-col-slider"  style="overflow:auto">
                            <div class="mv-block-style-26">
                                <div class="product-detail-slider mv-slick-slide mv-lightbox-style-1">
                                    <div class="block-26-gallery-main">
                                        <div class="slider gallery-main">
                                            

                                            <div class="slick-slide">
                                                <div class="slick-slide-inner">
                                                    <a href="<?= $detail->foto_portada ?>" title="" class="mv-lightbox-item">
                                                        <img src="<?= $detail->foto_portada ?>" alt="demo" class="block-26-main-img"/>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- .slick-slide-->

                                            <div class="slick-slide">
                                                <div class="slick-slide-inner">
                                                    <a href="<?= $detail->foto_hover ?>" title="" class="mv-lightbox-item">
                                                        <img src="<?= $detail->foto_hover ?>" alt="demo" class="block-26-main-img"/>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- .slick-slide-->
                                            <?php foreach($this->db->get_where('productos_fotos',array('productos_id'=>$detail->id))->result() as $f): ?>
                                                <div class="slick-slide">
                                                    <div class="slick-slide-inner">
                                                        <a href="<?= base_url('img/productos/'.$f->url) ?>" title="" class="mv-lightbox-item">
                                                            <img src="<?= base_url('img/productos/'.$f->url) ?>" alt="demo" class="block-26-main-img"/>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        </div>
                                    </div>
                                    <!-- .block-26-gallery-main-->

                                    <div class="block-26-gallery-thumbs">
                                        <div class="block-26-gallery-thumbs-inner">
                                            <div class="slider gallery-thumbs">
                                                
                                                <div class="slick-slide">
                                                    <div class="slick-slide-inner mv-box-shadow-gray-2">
                                                        <img src="<?= $detail->foto_portada ?>" alt="demo" class="block-26-thumbs-img"/>
                                                    </div>
                                                </div> 
                                                <div class="slick-slide">
                                                    <div class="slick-slide-inner mv-box-shadow-gray-2">
                                                        <img src="<?= $detail->foto_hover ?>" alt="demo" class="block-26-thumbs-img"/>
                                                    </div>
                                                </div>

                                            <?php foreach($this->db->get_where('productos_fotos',array('productos_id'=>$detail->id))->result() as $f): ?>
                                                <div class="slick-slide">
                                                    <div class="slick-slide-inner mv-box-shadow-gray-2">
                                                        <img src="<?= base_url('img/productos/'.$f->url) ?>" alt="demo" class="block-26-thumbs-img"/>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>                                                

                                            </div>

                                            <div class="slick-slide-control"></div>
                                        </div>
                                    </div>
                                    <!-- .block-26-gallery-thumbs-->
                                </div>
                                <!-- .product-detail-slider-->
                            </div>
                            <!-- .mv-block-style-26-->
                            <?php if(!empty($detail->descuento)): ?>
                                <div class="block-27-sale-off mv-label-style-2 text-center">
                                    <div class="label-2-inner">
                                        <div class="content-sale-off mv-label-style-3 label-dark"> 
                                            <div class="label-inner productodescuento"><?= $detail->descuento ?>%</div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>

                        </div>

                        <div class="mv-col-right block-27-col-info">
                            <div class="col-info-inner">
                                <div class="block-27-info">

                                    <div class="block-27-title" style="margin-bottom: 0px;">
                                        <?= $detail->productos_nombre ?>                                         
                                    </div>
                                    <div class="content-desc">
                                        <?= $detail->descripcion_corta ?>
                                        <div class="block-27-price"></div> 
                                    </div>
                                    <div class="content-sale-off mv-label-style-2 text-left"  style="text-align:left">
                                        <div class="label-2-inner">
                                            <ul class="label-2-ul">
                                                <li class="number">REF</li>
                                                <li class="text"><?= $detail->referencia ?></li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="block-27-price">
                                        <div class="new-price detailprecio">Consultar Precio</div>
                                    </div>
                                    <?= $detail->disponible==1?'<div class="mv-btn mv-btn-style-6 btn-6-instock">En Stock</div>':'<div class="mv-btn mv-btn-style-6 btn-6-out-of-stock">Agotado</div>'; ?>

                                    <div class="block-27-table-info">
                                        <form method="GET">
                                            <table>
                                                <?php foreach($this->db->get_where('caracteristicas',array('productos_id'=>$detail->id))->result() as $c): ?>
                                                    <tr>
                                                        <td><?= $c->nombre ?>:</td>
                                                        <td>
                                                            <div class="mv-list-inline-style-1">
                                                                <ul class="list-inline-1">
                                                                    <li class="active"><a href="#" class="mv-btn mv-btn-style-8"><?= $c->valor ?></a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                                <tr>
                                                    <td>Cantidad:</td>
                                                    <td>
                                                      <div class="mv-spinner-style-1 input-quantity-wrapper">
                                                        <input type="text" name="cantidad" value="1" class="mv-inputbox mv-only-numeric input-quantity-product-detail"/>
                                                      </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tags: </td>
                                                    <td>
                                                        <div class="mv-list-inline-style-2">
                                                            <ul class="list-inline-2">
                                                                <?php if(!empty($detail->tags)): ?>
                                                                    <?php foreach(explode(',',$detail->tags) as $t): ?>
                                                                        <li><a href="<?= base_url('productos/'.$detail->categoria_productos_id.'-conultar_tab?productos_nombre='.$t) ?>" class="mv-btn mv-btn-style-22"><?= $t ?></a></li>
                                                                    <?php endforeach ?>            
                                                                <?php endif ?>                                                                    
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                                <!-- .block-27-info-->

                                <div class="block-27-message content-message mv-message-style-1">
                                    <div class="message-inner"></div>
                                </div>
                                <!-- .block-27-message-->
                            </div>

                            <div class="block-27-button">
                                <div class="mv-dp-table align-middle">
                                    <div class="mv-dp-table-cell">
                                        <div class="mv-btn-group text-left">
                                            <div class="group-inner">
                                                <button type="button" class="mv-btn mv-btn-style-1 btn-1-h-50 responsive-btn-1-type-3 btn-add-to-cart" data-id="<?= $detail->id ?>" <?= $detail->disponible==0?'disabled="true"':'' ?>><span class="btn-inner"><i class="btn-icon fa fa-hand-o-right"></i><span class="btn-text">Añadir a mi presupuesto</span></span></button>
                                                <button type="button" class="mv-btn mv-btn-style-3 btn-3-h-50 responsive-btn-3-type-1 btn-add-to-wishlist" data-id="<?=  empty($_SESSION['user'])?0:$detail->id ?>"><i class="fa fa-heart-o"></i></button>
                                                <button type="button" class="mv-btn mv-btn-style-3 btn-3-h-50 responsive-btn-3-type-1 btn-compare" data-id="<?=  empty($_SESSION['user'])?0:$detail->id ?>"><i class="fa fa-signal"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .mv-block-style-27-->
            </div>
            <!-- .block-info-->

            <div class="row block-info-more">
                <div class="col-sm-12 col-specification">
                    <div class="specification-main mv-tab-style-3 mv-box-shadow-gray-1 mv-bg-white">
                        <ul role="tablist" class="tab-list nav nav-tabs">
                            <li role="presentation" class="active"><a href="#tab31" aria-controls="tab31" role="tab" data-toggle="tab">Descripción</a></li>
<!--                            <li role="presentation"><a href="#tab32" aria-controls="tab32" role="tab" data-toggle="tab">Especificación</a></li>-->
<!--                            <li role="presentation"><a href="#tab33" aria-controls="tab33" role="tab" data-toggle="tab">Información</a></li>-->
                        </ul>

                        <div class="tab-content">
                            <div id="tab31" role="tabpanel" class="tab-pane active">
                                <?= $detail->descripcion ?>
                            </div>
                            <div id="tab32" role="tabpanel" class="tab-pane">
                                <?= $detail->especificacion ?>
                            </div>
                            <div id="tab33" role="tabpanel" class="tab-pane">
                                <?= $detail->informacion ?>
                            </div>
                        </div>
                    </div>
                    <!-- .specification-main-->
                </div>
            </div>
            <!-- .block-info-more-->
        </div>
        <!-- .post-->
    </div>
</section>
<!-- .mv-main-body-->

<section class="product-detail-related mv-wrap">
    <div class="container">
        <div class="related-title mv-title-style-3">
            <div class="title-3-text"><span class="behind">Productos Relacionados</span><span class="main">Productos Relacionados</span></div>
        </div>
        <!-- .related-title-->

        <div class="related-main">
            <div class="related-list mv-block-style-9">
                <div class="block-9-list">
                    
                    <?php foreach($relacionados->result() as $r): ?>
                        <article class="col-xs-6 col-sm-4 col-md-3 item post">
                            <?php $this->load->view('frontend/_producto',array('p'=>$r)) ?>
                        </article>
                        <!-- .post-->
                    <?php endforeach ?>
                    
                </div>
            </div>
        </div>
        <!-- .related-main-->
    </div>
</section>
<!-- .product-detail-related-->
