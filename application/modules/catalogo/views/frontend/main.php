<section class="main-banner mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/demo_bg_1920x1680.png" class="mv-banner-style-1 mv-bg-overlay-dark overlay-0-85 mv-parallax">
        <div class="page-name mv-caption-style-6">
            <div class="container">
                <div class="mv-title-style-9">
                    <span class="main">Productos</span>
                    <img src="<?= base_url() ?>img/icon/icon_line_polygon_line.png" alt="icon" class="line"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- .main-banner-->
<section class="main-breadcrumb mv-wrap">
    <div class="mv-breadcrumb-style-1">
        <div class="container">
            <ul class="breadcrumb-1-list">
                <li><a href="<?= site_url() ?>"><i class="fa fa-home"></i></a></li>
                <li><a>Productos</a></li>
            </ul>
        </div>
    </div>
</section>
<!-- .main-breadcrumb-->
<section class="mv-main-body shortcodes-main mv-wrap">
    <div class="container">
        <div class="row accordion-shortcode">
            <div class="col-xs-12">
                <h2 class="mv-text-primary"><strong>ACCORDION</strong></h2>
                <p>Accordion are flexible and easy to use. It will be perfect to manage huge amount of content and bring a neat look. With Accordion shortcode, you can create as many accordions as you want, choose different icons and set active accordion when loading page. You can collapse all accordions at loading page by setting value -1 for active-tab.</p>
            </div>            
        </div>
        <!-- .accordion-shortcode-->
        <hr style="padding: 0 0 50px 0;" class="mv-gap">

        <div class="row tab-shortcode">
            <div class="col-xs-12">                
                <h3 class="mv-text-primary">Vertical Tab</h3>
                <div class="mv-tab-style-2">
                    <ul role="tablist" class="nav nav-tabs">
                        <li class=""><a role="tab" data-toggle="tab" href="#tab21" aria-expanded="false">Normal Tab</a></li>
                        <li class=""><a role="tab" data-toggle="tab" href="#tab22" aria-expanded="false">Active Tab</a></li>
                        <li class=""><a role="tab" data-toggle="tab" href="#tab23" aria-expanded="false">Hover Tab</a></li>
                        <li class="active"><a role="tab" data-toggle="tab" href="#tab24" aria-expanded="true"><i class="fa fa-music"></i> Tab With Icon</a></li>
                    </ul>
                    <!--.nav-tabs-->
                    <div class="tab-content">
                        <div id="tab21" class="tab-pane">
                            <p>Quisque tincidunt dictum ipsum quis cursus. Mauris et urna et turpis placerat egestas. Mauris ultricies risus eu massa gravida pulvinar. Aliquam quam diam, egestas in imperdiet a, aliquam ut odio. Vivamus nulla neque, scelerisque eu hendrerit ut, molestie eu sapien.</p>
                            <p>Aenean a ante lacus. Fusce eget lorem sollicitudin, adipiscing magna at, cursus orci. Morbi a rhoncus risus, ac porttitor arcu. Mauris dignissim elit sagittis, dictum mi sed, faucibus mi. Cras facilisis nibh dui, vitae sollicitudin sapien sagittis ac. Donec purus enim, posuere ac vestibulum vel, ultrices non mi. Sed condimentum suscipit leo, ut lacinia sem molestie ut. Nulla facilisi. Curabitur lobortis nec eros a pellentesque.</p>
                        </div>
                        <div id="tab22" class="tab-pane">
                            <p>Quisque tincidunt dictum ipsum quis cursus. Mauris et urna et turpis placerat egestas. Mauris ultricies risus eu massa gravida pulvinar. Aliquam quam diam, egestas in imperdiet a, aliquam ut odio. Vivamus nulla neque, scelerisque eu hendrerit ut, molestie eu sapien.</p>
                        </div>
                        <div id="tab23" class="tab-pane">
                            <p>Quisque tincidunt dictum ipsum quis cursus. Mauris et urna et turpis placerat egestas. Mauris ultricies risus eu massa gravida pulvinar. Aliquam quam diam, egestas in imperdiet a, aliquam ut odio. Vivamus nulla neque, scelerisque eu hendrerit ut, molestie eu sapien.</p>
                            <p>Aenean a ante lacus. Fusce eget lorem sollicitudin, adipiscing magna at, cursus orci. Morbi a rhoncus risus, ac porttitor arcu. Mauris dignissim elit sagittis, dictum mi sed, faucibus mi. Cras facilisis nibh dui, vitae sollicitudin sapien sagittis ac. Donec purus enim, posuere ac vestibulum vel, ultrices non mi. Sed condimentum suscipit leo, ut lacinia sem molestie ut. Nulla facilisi. Curabitur lobortis nec eros a pellentesque.</p>
                        </div>
                        <div id="tab24" class="tab-pane active">
                            <p>Aenean a ante lacus. Fusce eget lorem sollicitudin, adipiscing magna at, cursus orci. Morbi a rhoncus risus, ac porttitor arcu. Mauris dignissim elit sagittis, dictum mi sed, faucibus mi. Cras facilisis nibh dui, vitae sollicitudin sapien sagittis ac. Donec purus enim, posuere ac vestibulum vel, ultrices non mi. Sed condimentum suscipit leo, ut lacinia sem molestie ut. Nulla facilisi. Curabitur lobortis nec eros a pellentesque.</p>
                        </div>
                    </div>
                    <!-- .tab-content-->
                </div>
                <!-- .mv-tab-style-2-->
            </div>
        </div>
        <!-- .tab-shortcode-->
        <hr style="padding: 0 0 50px 0;" class="mv-gap">        
    </div>
</section>

<section class="main-newsletter mv-newsletter-style-1 mv-wrap">
    <div style="background-image: url(<?= base_url() ?>img/background/ayuda.jpg)" class="newsletter-bg">
        <div class="container">
            <form method="GET" class="form-newsletter">
                <label class="hidden-xs mv-label text-right text-uppercase">¿necesitas alguna ayuda?</label>
                <div class="mv-field">
                    <div class="mv-inputbox-group">
                        <input type="text" name="test138" placeholder="Subscribe your email here" data-mv-placeholder="Envianos un email y en un momento te contestamos" class="mv-inputbox mv-inputbox-style-3"/>
                        <div class="inputbox-addon">
                            <button type="button" class="mv-btn mv-btn-block mv-btn-style-13"><span class="btn-icon fa fa-long-arrow-right"></span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- .mv-newsletter-style-1-->



<section class="mv-main-body product-grid-3-main mv-bg-gray mv-wrap">
    <div class="container">
        <div class="row mv-content-sidebar">
            <?= $output ?>
            <!-- .mv-c-s-content-->

            <?php $this->load->view('frontend/_sidebar'); ?>
        </div>
    </div>
</section>
<!-- .mv-main-body-->
