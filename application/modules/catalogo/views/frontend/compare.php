<section class="main-banner mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/demo_bg_1920x1680.png" class="mv-banner-style-1 mv-bg-overlay-dark overlay-0-85 mv-parallax">
        <div class="page-name mv-caption-style-6">
            <div class="container">
                <div class="mv-title-style-9"><span class="main">Compare</span><img src="<?= base_url() ?>img/icon/icon_line_polygon_line.png" alt="icon" class="line"/></div>
            </div>
        </div>
    </div>
</section>
<!-- .main-banner-->

<section class="main-breadcrumb mv-wrap">
    <div class="mv-breadcrumb-style-1">
        <div class="container">
            <ul class="breadcrumb-1-list">
                <li>
                    <a href="<?= site_url() ?>">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><a>Comparar</a></li>
            </ul>
        </div>
    </div>
</section>
<!-- .main-breadcrumb-->

<section class="mv-main-body compare-main mv-bg-gray mv-wrap">
    <div class="container">
        <div class="compare-inner mv-bg-white mv-box-shadow-gray-1">
            <div class="mv-block-style-41">
                <div class="mv-table-responsive">
                    <?php if($productos->num_rows()>0): ?>
                    <table class="mv-table-style-3">
                        <tr>
                            <td class="table-2-label">Fotos del producto</td>
                            
                            <?php foreach($productos->result() as $p): ?>
                                <td class="content-thumb mv-effect-one-by-one">
                                    <div class="thumb-inner mv-effect-relative">
                                        <img src="<?= $p->foto_portada ?>" alt="demo" class="mv-effect-item active"/>
                                        <img src="<?= $p->foto_hover ?>" alt="demo" class="mv-effect-item"/>
                                    </div>
                                </td>
                            <?php endforeach ?>
                        </tr>
                        <tr>
                            <td class="table-2-label">Nombre</td>
                            <?php foreach($productos->result() as $p): ?>
                                <td class="content-thumb mv-effect-one-by-one">
                                    <a href="<?= $p->link ?>" title="<?= $p->productos_nombre ?>">
                                        <?= $p->productos_nombre ?>
                                    </a>
                                </td>
                            <?php endforeach ?>
                        </tr>                  
                        <tr>
                            <td class="table-2-label">Precio</td>
                            <?php foreach($productos->result() as $p): ?>
                                <td class="content-price"><strong class="mv-color-primary mv-f-18">Consultar</strong></td>
                            <?php endforeach ?>
                        </tr>
                        
                        <tr>
                            
                            <td class="table-2-label">Caracteristicas</td>
                            <?php foreach($productos->result() as $p): ?>
                                <td class="content-price"><strong class="mv-color-primary mv-f-18">
                                    <?php foreach($this->db->get_where('caracteristicas',array('productos_id'=>$p->id))->result() as $c): ?>
                                        <?= $c->nombre ?>: <?= $c->valor ?> |</strong>
                                    <?php endforeach ?>
                                </td>
                            <?php endforeach ?>
                        </tr>
                        
                        
                        <tr>
                            <td class="table-2-label">Disponibilidad</td>
                            <?php foreach($productos->result() as $p): ?>
                                <td class="content-price"><?= $p->disponible==1?'<div class="mv-btn mv-btn-style-6 btn-6-instock">En Stock</div>':'<div class="mv-btn mv-btn-style-6 btn-6-out-of-stock">Agotado</div>'; ?></td>
                            <?php endforeach ?>
                        </tr>
                        <tr>
                            <td class="table-2-label">Action</td>
                            <?php foreach($productos->result() as $p): ?>
                                <td class="content-button">
                                  <div class="mv-btn-group text-center">
                                    <div class="post group-inner">
                                      <button type="button" class="mv-btn mv-btn-style-3 responsive-btn-3-type-1 btn-add-to-cart" data-id="<?= $p->id ?>" <?= $p->disponible==0?'disabled="true"':'' ?>><i class="btn-icon fa fa-hand-o-right"></i></button>
                                      <button type="button" class="mv-btn mv-btn-style-3 responsive-btn-3-type-1 btn-add-to-wishlist" data-id="<?= $p->id ?>"><i class="fa fa-heart-o"></i></button>
                                      <button type="button" class="mv-btn mv-btn-style-3 responsive-btn-3-type-1 btn-del-to-compare" data-id="<?= $p->id ?>"><i class="fa fa-close"></i></button>
                                      <div class="content-message">
                                        <div class="message-inner"></div>
                                      </div>
                                    </div>
                                  </div>
                                </td>
                            <?php endforeach ?>
                          </tr>
                    </table>
                    <?php else: ?>
                    Sin productos añadidos a la lista
                    <?php endif ?>
                </div>
            </div>
            <!-- .mv-block-style-41-->
        </div>
    </div>
</section>
<!-- .mv-main-body-->
