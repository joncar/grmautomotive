<?php
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');
$this->set_js_lib('assets/grocery_crud/js/common/list.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/cookies.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/flexigrid.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/pagination.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>
<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="wishlist-inner mv-bg-white mv-box-shadow-gray-1 flexigrid">
    <div class="mv-table-responsive">        
        <?php echo $list_view ?>
    </div>
    
    <div class="mv-pagination-wrapper">
        <div class="mv-pagination-style-1 has-count-post">
            <div class="count-post mv-title-style-10">
                <span class="number">
                    <?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                    <?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>" . ($total_results < $default_per_page ? $total_results : $default_per_page) . "</span>"; ?>
                    <?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>" ?>
                    <?php
                    echo str_replace(array('{start}', '{end}', '{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results), $this->l('list_displaying')
                    );
                    ?>
                </span>
            </div>
            <ul class="pagination">

            </ul>
        </div>
        <!-- .mv-pagination-style-1-->
    </div>
    <!-- .mv-pagination-wrapper-->
</div>

<div class="ajax_refresh_and_loading"></div>
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<input name='page' type="hidden" value="1" size="4" id='page'>
<input name='per_page' type="hidden" value="9" size="4">
<?php echo form_close() ?>
