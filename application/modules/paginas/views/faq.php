<section class="main-banner mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/faq.jpg" class="mv-banner-style-1 mv-bg-overlay-dark overlay-0-85 mv-parallax">
        <div class="page-name mv-caption-style-6">
            <div class="container">
                <div class="mv-title-style-9"><span class="main">Faqs</span><img src="<?= base_url() ?>img/icon/icon_line_polygon_line.png" alt="icon" class="line"/></div>
            </div>
        </div>
    </div>
</section>
<!-- .main-banner-->

<section class="main-breadcrumb mv-wrap">
    <div class="mv-breadcrumb-style-1">
        <div class="container">
            <ul class="breadcrumb-1-list">
                <li><a href="home.html"><i class="fa fa-home"></i></a></li>
                <li><a>Faqs</a></li>
            </ul>
        </div>
    </div>
</section>
<!-- .main-breadcrumb-->

<section class="mv-main-body faqs-main mv-bg-gray mv-wrap">
    <div class="container">
        <div class="faqs-inner mv-box-shadow-gray-1 mv-bg-white">
            <div class="faqs-box mv-accordion-style-3">
                <div id="accordion-faq" role="tablist" aria-multiselectable="true" class="panel-group">
                    <div class="panel panel-default">
                        <div role="tab" class="panel-heading"><a role="button" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse-faq-1" aria-expanded="true">Do I need to register before placing an order?</a></div>
                        <div id="collapse-faq-1" role="tabpanel" class="panel-collapse collapse in">
                            <div class="panel-body">Yes you do need to register before placing an order. Our registration process is fast, free, and will save you time for future purchases. Click here to register.</div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div role="tab" class="panel-heading"><a role="button" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse-faq-2" aria-expanded="false" class="collapsed">How do I change my account information?</a></div>
                        <div id="collapse-faq-2" role="tabpanel" class="panel-collapse collapse">
                            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione nesciunt reiciendis quae illo tempora ex non, quod sequi enim obcaecati saepe exercitationem sed blanditiis. Neque quasi nostrum totam eveniet vel!</div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div role="tab" class="panel-heading"><a role="button" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse-faq-3" aria-expanded="false" class="collapsed">Why stop shopping when you can receive free shipping? </a></div>
                        <div id="collapse-faq-3" role="tabpanel" class="panel-collapse collapse">
                            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id dicta, totam quasi repudiandae, doloremque earum hic, voluptas laborum ullam nisi aliquid harum ratione quaerat necessitatibus suscipit eligendi ipsa iste, quis!</div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div role="tab" class="panel-heading"><a role="button" data-toggle="collapse" data-parent="#accordion-faq" href="#collapse-faq-4" aria-expanded="false" class="collapsed">Can I pay with more than one motovehikle Gift Cards, E-Gift Certificates, and Store Credits?</a></div>
                        <div id="collapse-faq-4" role="tabpanel" class="panel-collapse collapse">
                            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id dicta, totam quasi repudiandae, doloremque earum hic, voluptas laborum ullam nisi aliquid harum ratione quaerat necessitatibus suscipit eligendi ipsa iste, quis!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- .mv-main-body-->