<section class="main-banner mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/contact.jpg" class="mv-banner-style-1 mv-bg-overlay-dark overlay-0-85 mv-parallax">
        <div class="page-name mv-caption-style-6">
            <div class="container">
                <div class="mv-title-style-9">
                    <span class="main">Contactenos</span>
                    <img src="<?= base_url() ?>img/icon/icon_line_polygon_line.png" alt="icon" class="line"/>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- .main-banner-->

<section class="main-breadcrumb mv-wrap">
    <div class="mv-breadcrumb-style-1">
        <div class="container">
            <ul class="breadcrumb-1-list">
                <li><a href="<?= site_url() ?>"><i class="fa fa-home"></i></a></li>
                <li><a>Contáctenos</a></li>
            </ul>
        </div>
    </div>
</section>
<!-- .main-breadcrumb-->

<section class="mv-main-body contact-us-main mv-bg-gray mv-wrap">
    <div class="contact-us-form">
        <div class="container">
            <div class="mv-block-style-32">
                <div class="block-32-form mv-box-shadow-gray-1">
                    <div class="block-32-form-title mv-title-style-3">
                        <div class="title-3-text">
                            <span class="behind">Formulario de contacto</span>
                            <span class="main">Formulario de contacto</span>
                    </div>
                    </div>
                    <!-- .block-32-form-title-->

                    <div class="block-32-form-main">
                        <?php if(!empty($_SESSION['msj'])): ?>
                            <div><?= $_SESSION['msj'] ?></div>
                            <?php unset($_SESSION['msj']); ?>
                        <?php endif ?>
                        <form action="<?= base_url('paginas/frontend/contacto') ?>" method="post" class="form-contact mv-form-horizontal">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="mv-form-group">
                                        <div class="col-md-2 mv-label"> <strong class="text-uppercase">Nombre</strong></div>
                                        <div class="col-md-10 mv-field">
                                            <input type="text" name="nombre" class="mv-inputbox mv-inputbox-style-1"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="mv-form-group">
                                        <div class="col-md-2 mv-label"> <strong class="text-uppercase">E-mail</strong></div>
                                        <div class="col-md-10 mv-field">
                                            <input type="email" name="email" class="mv-inputbox mv-inputbox-style-1"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="mv-form-group">
                                        <div class="col-md-2 mv-label"> <strong class="text-uppercase">Titulo</strong></div>
                                        <div class="col-md-10 mv-field">
                                            <input type="text" name="titulo" class="mv-inputbox mv-inputbox-style-1"/>
                                        </div>
                                    </div>
                                </div>

                            <div class="mv-form-group input-message-wrapper">
                                <div class="col-md-1 mv-label"> <strong class="text-uppercase"> </strong></div>
                                <div class="col-md-11 mv-field">
                                    <textarea name="comentario" rows="6" placeholder="Escribe un Mensaje" data-mv-placeholder="Escribe un Mensaje" class="mv-inputbox mv-inputbox-style-1"></textarea>
                                </div>
                            </div>

                            <div class="mv-form-group submit-button">
                                <div class="col-md-1 mv-label"> <strong class="text-uppercase"> </strong></div>
                                <div class="col-md-11 mv-field">
                                    <button type="submit" class="mv-btn mv-btn-style-5 btn-5-h-50 mv-btn-block btn-subscribe">Enviar Mensaje</button>
                                </div>
                            </div>
                        </form>
                        <!-- .form-contact-->
                    </div>
                    <!-- .block-32-form-main-->
                </div>
                <!-- .block-32-form-->


                <!-- .block-32-contact-->
            </div>
            <div class="block-32-contact mv-box-shadow-gray-1">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="contact-box">
                            <div class="mv-icon-left-style-7">
                                <div class="i-l-inner">
                                    <div class="i-l-text"><span class="i-l-icon icon-map-marker fa fa-map-marker mv-f-54 mv-color-primary"></span>
                                        <div class="text-main">dirección</div>
                                        <div class="text-sub">Canifa Shop Center 85 Fentiman Ave</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="contact-box">
                            <div class="mv-icon-left-style-7">
                                <div class="i-l-inner">
                                    <div class="i-l-text"><span class="i-l-icon icon-mobile fa fa-mobile mv-f-60 mv-color-primary"></span>
                                        <div class="text-main">teléfono</div>
                                        <div class="text-sub">

                                            <a href="tel:1-800-64-38"> +34 607 719 378 </a><br/>Whatsapp <br><a href="tel:1-800-64-39"> +34 607 719 378 </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="contact-box">
                            <div class="mv-icon-left-style-7">
                                <div class="i-l-inner">
                                    <div class="i-l-text"><span class="i-l-icon icon-envelope fa fa-envelope-o mv-f-48 mv-color-primary"></span>
                                        <div class="text-main">e-mail</div>
                                        <div class="text-sub"> <a href="mailto:info@motor.com">info@grmautomotive.eu</a><br/><a href="mailto:motor@shoponline.com">reservas@grmautomotive.eu</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .mv-block-style-32-->
        </div>
        <!-- .contact-us-form-->
    </div>

    <div class="contact-us-map">
        <div class="map-embed">
            <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBoW1mnkomGhsB2yL--AYoFdnE-jkgskSI&amp;language=es;"></script>
            <div id="mv-map-canvas" data-latitude="41.58838956202096" data-longitude="1.6303431987762451"   data-address="	C/ Gran Bretaña, 22 Nave 3 08700 IGUALADA - BCN- Spain-"></div>
        </div>
    </div>
    <!-- .contact-us-map-->
</section>
<!-- .mv-main-body-->
