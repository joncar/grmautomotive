<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Añadir paginas</h1>
    </div>
    <div class="panel-body">
        <?php if(!empty($_GET['msj']))echo '<div class="alert alert-danger">'.$_GET['msj'].'</div>'; ?>
        <form method="post" action="<?= base_url('paginas/admin/paginas/insert') ?>">
            <div class="form-group">
              <label for="text">Nombre del archivo* </label>
              <input type="text" name="nombre" class="form-control" id="text" placeholder="Nombre de archivo">
            </div> 
            <div class="form-group">
              <label for="text">Columnas* </label>
              <select class="form-control" name="columnas">
                  <option value="1">1 Columna</option>
                  <option value="2">2 Columnas</option>
                  <option value="3">3 Columnas</option>
              </select>
            </div>
            <button type="submit" class="btn btn-default">Añadir y editar</button>
        </form>
    </div>
</div>