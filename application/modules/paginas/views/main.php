<section class="home-2-slideshow mv-wrap">
    <div id="home-2-slideshow" class="mv-caroufredsel">
        <ul class="mv-slider-wrapper">
            <li class="mv-slider-item"><img src="<?= base_url() ?>img/slideshow/slide1.jpg" alt="slide" class="mv-slider-img"/>
                <div class="mv-caption-style-2">
                    <div class="container">
<!--                        <img src="--><?//= base_url() ?><!--img/icon/icon_stars.png" alt="icon" class="caption-2-img-1"/>-->
                        <div class="caption-2-text-1">Todo para tus ruedas</div>
                        <div class="caption-2-text-2">contrapesas • valvulas • herramientas</div>
                    </div>
                </div>
            </li>
            <!-- .mv-slider-item-->

            <li class="mv-slider-item"><img src="<?= base_url() ?>img/slideshow/slide2.jpg" alt="slide" class="mv-slider-img"/>
                <div class="mv-caption-style-2">
                    <div class="container">
<!--                        <img src="--><?//= base_url() ?><!--img/icon/icon_stars.png" alt="icon" class="caption-2-img-1"/>-->
                        <div class="caption-2-text-1">Alineaciones perfectas</div>
                        <div class="caption-2-text-2">maquinaria • accesorios • reparación</div>
                    </div>
                </div>
            </li>
            <!-- .mv-slider-item-->

            <li class="mv-slider-item"><img src="<?= base_url() ?>img/slideshow/slide3.jpg" alt="slide" class="mv-slider-img"/>
                <div class="mv-caption-style-2">
                    <div class="container">
<!--                        <img src="--><?//= base_url() ?><!--img/icon/icon_stars.png" alt="icon" class="caption-2-img-1"/>-->
                        <div class="caption-2-text-1">Soluciones para tu taller</div>
                        <div class="caption-2-text-2">contrapesas • Maquinnaria • valvulas</div>
                    </div>
                </div>
            </li>
            <!-- .mv-slider-item-->
        </ul>
    </div>
</section>
<!-- .home-2-slideshow-->

<section class="home-2-highlight home-5-highlight mv-wrap">
    <div class="container">
        <div class="block-our-team mv-bg-white">
            <div class="container">                

                <div class="block-our-team-main">
                    <div class="mv-block-style-35">
                        <div class="row block-35-list">

                            <div class="col-xs-6 col-sm-4 item">
                                <div class="item-inner">
                                    <a href="<?= site_url('blog') ?>" class="block-35-link">
                                        <span class="block-35-default">
                                            <span style="background-image: url(http://grmautomotive.eu/test/img/demo/demo_370x530.png);" class="block-35-bg">
                                                <span class="block-35-solid-box"></span>
                                                <span class="block-35-text">
                                                    <span class="block-35-title">Noticias</span>
                                                    <span class="block-35-decs">Informació del Sector</span>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="block-35-hover">
                                            <span style="background-image: url(<?= base_url() ?>img/demo/demo_370x530.png);" class="block-35-bg">
                                                <img src="<?= base_url() ?>img/icon/icon_plus.png" alt="icon" class="block-35-plus"/>
                                                <span class="block-35-text">
                                                    <span class="block-35-title">Noticias</span>
                                                    <span class="block-35-decs">Informació del Sector</span>
                                                </span>
                                            </span> 
                                        </span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-4 item">
                                <div class="item-inner">
                                    <a href="<?= site_url('catalogo') ?>" class="block-35-link">
                                        <span class="block-35-default">
                                            <span style="background-image: url(<?= base_url() ?>img/demo/catalogo.jpg);" class="block-35-bg">
                                                <span class="block-35-solid-box"></span>
                                                <span class="block-35-text">
                                                    <span class="block-35-title">Catálogo</span>
                                                    <span class="block-35-decs">Toda la gama de productos</span>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="block-35-hover">
                                            <span style="background-image: url(<?= base_url() ?>img/demo/catalogo.jpg);" class="block-35-bg">
                                                <img src="<?= base_url() ?>img/icon/icon_plus.png" alt="icon" class="block-35-plus"/>
                                                <span class="block-35-text">
                                                    <span class="block-35-title">Catálogo</span>
                                                    <span class="block-35-decs">Toda la gama de productos</span>
                                                </span>
                                            </span> 
                                        </span>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="col-xs-6 col-sm-4 item">
                                <div class="item-inner">
                                    <a href="<?= site_url('p/faq') ?>" class="block-35-link">
                                        <span class="block-35-default">
                                            <span style="background-image: url(<?= base_url() ?>img/demo/faq.jpg);" class="block-35-bg">
                                                <span class="block-35-solid-box"></span>
                                                <span class="block-35-text">
                                                    <span class="block-35-title">FAQ</span>
                                                    <span class="block-35-decs">Como funcionamos</span>
                                                </span>
                                            </span>
                                        </span>
                                        <span class="block-35-hover">
                                            <span style="background-image: url(<?= base_url() ?>img/demo/faq.jpg);" class="block-35-bg">
                                                <img src="<?= base_url() ?>img/icon/icon_plus.png" alt="icon" class="block-35-plus"/>
                                                <span class="block-35-text">
                                                    <span class="block-35-title">FAQ</span>
                                                    <span class="block-35-decs">Como funcionamos</span>
                                                </span>
                                            </span> 
                                        </span>
                                    </a>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                    <!-- .mv-block-style-35-->
                </div>
                <!-- .block-our-team-main-->
            </div>
        </div>
        <!-- .mv-block-style-20-->
    </div>
</section>
<!-- .home-5-highlight-->




<section class="home-4-service mv-wrap">
    <div class="container">
        <div class="what-hots-main">
            <div class="mv-block-style-19">
                <div class="row block-19-list">
                    <div class="col-xs-6 col-md-3 item">
                        <div class="item-inner">
                            <div class="block-19-title">
                                <div class="mv-icon-left-style-4">
                                    <a href="#">
                                        <span class="text">
                                            <span class="icon">
                                                <i class="fa fa-truck"></i>
                                            </span>
                                            entrega inmediata
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="block-19-decs">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste optio at, quisquam dolore esse consequuntur tempora placeat, voluptas. Asperiores tempora dignissimos ex unde eligendi libero vitae vero hic alias ullam.
                            </div>
                            <div class="block-19-line"></div>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-3 item">
                        <div class="item-inner">
                            <div class="block-19-title">
                                <div class="mv-icon-left-style-4">
                                    <a href="#">
                                        <span class="text">
                                            <span class="icon">
                                                <i class="fa fa-gift"></i>
                                            </span>
                                            Descuentos en tu pedido
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="block-19-decs">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste optio at, quisquam dolore esse consequuntur tempora placeat, voluptas. Asperiores tempora dignissimos ex unde eligendi libero vitae vero hic alias ullam.
                            </div>
                            <div class="block-19-line"></div>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-3 item">
                        <div class="item-inner">
                            <div class="block-19-title">
                                <div class="mv-icon-left-style-4">
                                    <a href="#">
                                        <span class="text">
                                            <span class="icon">
                                                <i class="fa fa-motorcycle"></i>
                                            </span>
                                            Devolución en 10 dias
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="block-19-decs">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste optio at, quisquam dolore esse consequuntur tempora placeat, voluptas. Asperiores tempora dignissimos ex unde eligendi libero vitae vero hic alias ullam.
                            </div>
                            <div class="block-19-line"></div>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-3 item">
                        <div class="item-inner">
                            <div class="block-19-title">
                                <div class="mv-icon-left-style-4">
                                    <a href="#">
                                        <span class="text">
                                            <span class="icon">
                                                <i class="fa fa-shield"></i>
                                            </span>Seguridad en el pago
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="block-19-decs">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste optio at, quisquam dolore esse consequuntur tempora placeat, voluptas. Asperiores tempora dignissimos ex unde eligendi libero vitae vero hic alias ullam.
                            </div>
                            <div class="block-19-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .mv-block-style-19-->
        </div>
        <!-- .what-hots-main-->
    </div>
</section>
<!-- .home-4-service-->


<section class="mv-main-body home-lookbook-1-main mv-wrap">
    <div class="container-fluid">
        <div class="mv-title-style-3 color-dark" style="margin-bottom:30px">
            <div class="title-3-text">
                <span class="behind">Nuevos productos</span>
                <span class="main">Nuevos Productos</span>
                <span class="sub">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</span>
            </div>
            <div class="title-3-line"></div>
        </div>



        <?php foreach($categoria_productos->result() as $n=>$c): ?>
            <div class="block-style">
                <div class="mv-block-style-9">
                    <div class="row block-9-list" style="margin-left:12px; margin-right: 13px;">
                        <?php if($n%2==0): ?>
                            <article class="col-xas-6 col-sm-4 col-md-3 item post">
                                <div class="mv-block-style-21">
                                    <div class="block-21-thumb">
                                        <div class="block-21-thumb-border hidden-xs hidden-sm"></div>
                                        <div class="block-21-thumb-main">
                                            <div style="background-image: url(<?= $c->foto ?>);" class="block-21-thumb-img">
                                                <a href="<?= site_url('productos/'.toURL($c->id.'-'.$c->categoria_nombre)) ?>" title="<?= $c->categoria_nombre ?>">
                                                    <span class="block-21-title-2 mv-title-style-8">
                                                        <span class="title-8-text"><span class="behind"><?= $c->categoria_nombre ?></span>
                                                            <span class="main-wrapper">
                                                                <span class="main"><?= $c->categoria_nombre ?></span>
                                                                <span class="sub"><?= $c->descripcion ?></span>
                                                                <span class="catallproducts">Todos los productos<span class="catallproductss"></span></span>
                                                            </span>
                                                        </span>  
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <!-- .post-->
                        <?php endif ?>                        
                        <?php foreach($c->productos->result() as $p): ?>
                            <article class="col-xs-6 col-sm-4 col-md-3 item post">
                                <?php $this->load->view('frontend/_producto',array('p'=>$p,'hidden_descuento'=>true),FALSE,'catalogo'); ?>
                            </article>
                        <?php endforeach ?>
                        <!-- .post-->
                        <?php if($n%2!=0): ?>
                            <article class="col-xas-6 col-sm-4 col-md-3 item post">
                                <div class="mv-block-style-21">
                                    <div class="block-21-thumb">
                                        <div class="block-21-thumb-border hidden-xs hidden-sm"></div>
                                        <div class="block-21-thumb-main">
                                            <div style="background-image: url(<?= $c->foto ?>);" class="block-21-thumb-img">
                                                <a href="<?= site_url('productos/'.toURL($c->id.'-'.$c->categoria_nombre)) ?>" title="RST Full Zipped Sweatshirt">
                                                    <span class="block-21-title-2 mv-title-style-8">
                                                        <span class="title-8-text"
                                                              ><span class="behind"><?= $c->categoria_nombre ?></span>
                                                            <span class="main-wrapper">
                                                                <span class="main"><?= $c->categoria_nombre ?></span>
                                                                <span class="sub"><?= $c->descripcion ?></span>
                                                                <span class="catallproducts">Todos los productos<span class="catallproductss"></span></span>
                                                            </span>
                                                        </span>  
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <!-- .post-->
                        <?php endif ?>


                    </div>
                </div>
                <!-- .mv-block-style-9-->                
            </div>
        <?php endforeach ?>
  <!--      <div style="text-align:center; margin-bottom:30px;">
            <a href="<?/*= base_url('catalogo') */?>" class="mv-btn mv-btn-style-1 btn-1-h-40 btn-1-gray responsive-btn-1-type-2 btn-shop-now hidden-xs hidden-sm">
                <span class="btn-inner">
                    <i class="btn-icon fa fa-eye"></i>
                    <span class="btn-text">Más productos</span>
                        
                </span>
            </a>
        </div>-->
        
    </div>   
</section>
<!-- .mv-main-body-->


<section class="home-1-deals-last-week mv-wrap">
    <div data-image-src="<?= base_url() ?>img/background/demo_bg_1920x1680.jpg" class="deals-last-week-bg mv-parallax">
        <div class="container">
            <div class="deals-last-week-title mv-title-style-2 color-white">
                <div class="title-2-inner">                    
                    <img src="<?= base_url() ?>img/icon/icon_m_2.png" alt="icon" class="icon image-live-view"/>
                    <span class="main">Ofertas del mes</span>
                    <span class="sub">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod  
                        <br/>tincidunt ut laoreet dolore magna aliquam erat volutpat
                    </span>
                </div>
            </div>
            <!-- .deals-last-week-title-->

            <div class="deals-last-week-main">
                <div class="deals-last-week-list">
                    <div class="mv-block-style-9">
                        <div class="row block-9-list">                              
                            <?php foreach($ofertas->result() as $p): ?>
                                <article class="col-xs-6 col-md-3 item post item-bg-white">
                                    <?php $this->load->view('frontend/_producto',array('p'=>$p),FALSE,'catalogo'); ?>
                                </article>
                            <!-- .post-->
                            <?php endforeach ?>                            
                            <!-- .post-->
                        </div>
                    </div>
                    <!-- .mv-block-style-2-->
                </div>
            </div>
            <!-- .deals-last-week-main-->
        </div>
    </div>
</section>
<!-- .home-1-deals-last-week-->

<section class="home-1-latest-blog mv-wrap" style="margin-top:40px;">
    <div class="container">
        <div class="latest-blog-title mv-title-style-2">
            <div class="title-2-inner">
                <img src="<?= base_url() ?>img/icon/icon_m.png" alt="icon" class="icon image-live-view"/>
                <span class="main">Ultimas Noticias</span>
                <span class="sub">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</span>
            </div>
        </div>
        <!-- .latest-blog-title-->

        <div class="latest-blog-main">
            <div class="latest-blog-list">
                <div class="mv-block-style-4">
                    <div class="row block-4-list">
                        
                        <?php foreach($blog->result() as $b): ?>
                        <article class="col-xs-6 col-sm-12 item post">
                            <div class="item-inner mv-effect-zoom-in-out">
                                <div class="block-4-thumb">
                                    <div class="row">
                                                <div class="col-sm-6">
                                                    <span class="block-4-thumb-inner">
                                                        <a href="<?= $b->link ?>" title="<?= $b->titulo ?>">
                                                            <span style="background-image: url(<?= $b->foto ?>);" class="block-4-thumb-img"></span>
                                                        </a>
                                                        <span class="polygon hidden-xs"></span>
                                                        <span class="block-4-date mv-date-style-1">
                                                            <span class="day"><?= strftime("%d",strtotime($b->fecha)); ?></span>
                                                            <span class="month"><?= strftime("%b",strtotime($b->fecha)); ?></span>                                                                
                                                        </span>                                                            
                                                    </span>
                                                </div>
                                        <div class="col-sm-6"></div>
                                    </div>
                                </div>
                                <!-- .block-4-thumb-->

                                <div class="block-4-main">
                                            <div class="block-4-main-inner">
                                                <a href="<?= $b->link ?>" title="<?= $b->titulo ?>" class="block-4-title mv-overflow-ellipsis">
                                                    <?= $b->titulo ?>
                                                </a>
                                                <div class="block-4-content">
                                                    <?= substr(strip_tags($b->texto),0,255).'...' ?>
                                    </div>
                                                <div class="block-4-read-more">
                                                    <a href="<?= $b->link ?>" class="mv-btn mv-btn-style-1 btn-1-h-40 btn-1-gray responsive-btn-1-type-5 btn-read-more">
                                                        <span class="btn-inner"><i class="btn-icon fa fa-angle-right"></i><span class="btn-text">Leer mas</span></span>
                                                    </a>
                                </div>
                            </div>
                                    </div>
                                <!-- .block-4-main-->
                            </div>
                        </article>
                        <!-- .item-->
                        <?php endforeach ?>
                        
                    </div>
                </div>
                <!-- .mv-block-style-4-->
            </div>
        </div>
        <!-- .latest-blog-main-->
    </div>
</section>
<!-- .home-1-latest-blog-->

<section class="home-3-logo-brand mv-wrap">
    <div class="container">
        <div class="logo-brand-main">
            <div class="mv-block-style-17">
                <div id="home-3-logo-brand-slider" class="home-3-logo-brand-slider mv-caroufredsel">
                    <div class="block-17-slider-inner">
                        <div class="row">
                            <ul class="mv-slider-wrapper">
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_2100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_3100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_4100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_5100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_6100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_7100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_8100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_9100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                                <li class="mv-slider-item col-xs-6 col-sm-4 col-md-2">
                                    <div class="mv-dp-table align-middle slider-item-inner">
                                        <div class="mv-dp-table-cell"><a href="#" target="_blank"><img src="<?= base_url() ?>img/logo-brand/logo_brand_10100x100.png" alt="logo_brand"/></a></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- .block-17-main-inner-->

                        <button id="home-3-logo-brand-slider-prev" type="button" class="mv-slider-control-btn prev mv-btn mv-btn-style-3 responsive-btn-3-type-1 hidden-lg"><i class="btn-icon fa fa-angle-left"></i></button>
                        <button id="home-3-logo-brand-slider-next" type="button" class="mv-slider-control-btn next mv-btn mv-btn-style-3 responsive-btn-3-type-1 hidden-lg"><i class="btn-icon fa fa-angle-right"></i></button>
                    </div>
                    <!-- .block-17-main-->
                </div>
                <!-- .home-3-logo-brand-slider-->
            </div>
            <!-- .mv-block-style-17-->
        </div>
    </div>
</section>
